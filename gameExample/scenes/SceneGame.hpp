#ifndef ECSGAMELOOP_GAMESCENE_HPP
#define ECSGAMELOOP_GAMESCENE_HPP


#include <engine/Scene.hpp>
#include <engine/core_system/Sound.hpp>

namespace scenes {
class SceneGame : public engine::Scene {
public:
  SceneGame(engine::Game &game);
  void Init() override;
  void Finish() override;

  void Start() override;
  void Update(float s) override;
  void ProcessInput(float dt);

private:
  std::shared_ptr<engine::Sound> _soundStart;
  std::function<engine::Entity(engine::Registry&)> _ballPrototype;

  void InitEntts();
  void InitSystems();
};
}

#endif //ECSGAMELOOP_GAMESCENE_HPP
