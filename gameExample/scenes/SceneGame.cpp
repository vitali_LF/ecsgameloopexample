#include "SceneGame.hpp"
#include "../ecs/components/Components.hpp"
#include "../ecs/systems/Systems.hpp"
#include <engine/core_system/Input.hpp>
#include <engine/FrameworkIncludes.hpp>

void GenerateRandomVelocity(engine::Registry& registry, engine::Entity entity) {
  auto ballVelRelToRes = engine::Config::Instance().ballVelocityRelativeToRes;
  float randomX = static_cast<float>(std::rand()) / (static_cast <float> (RAND_MAX / 2)) - 1;
  float randomY = static_cast<float>(std::rand()) / (static_cast <float> (RAND_MAX / 2)) - 1;
  registry.emplace_or_replace<engine::ecs::components::Velocity>(entity, randomX * ballVelRelToRes,
                                                                 randomY * ballVelRelToRes);
}

namespace scenes {
SceneGame::SceneGame(engine::Game& game) : engine::Scene(game, "game") {}

void SceneGame::Update(float dt) {
  if (!_running) return;
  engine::Scene::Update(dt);
  ProcessInput(dt);
}

void SceneGame::Start() {
  Scene::Start();
  _soundStart->Play();
}

void SceneGame::Init() {
  std::cout << "Start Game" << '\n';
  _soundStart = std::make_shared<engine::Sound>("sounds/start.wav");
  InitEntts();
  InitSystems();
}

void SceneGame::Finish() {
  std::cout << "Finish Game" << '\n';
  for (auto entity: _registry.view<ecs::components::Ball>()) {
    _registry.destroy(entity);
  }
}

void SceneGame::InitSystems() {
  AddSystem(std::make_shared<engine::ecs::systems::ParseSprite>(_game, _registry, "resources/mario_standing.png"));
  AddSystem(std::make_shared<engine::ecs::systems::RenderShadows2D>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::Render>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::AiPaddleControl>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::ApplyVelocity>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::CollisionSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::Drag>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::MissBallSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::AnimationStarterSystem>(_game, _registry, *this));
  AddSystem(std::make_shared<engine::ecs::systems::CameraRotationSystem>(_game, _registry, *this));
  AddSystem(std::make_shared<ecs::systems::PowerUpDetectionSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::DestroyOnCollisionSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::Score>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::GameOver>(_game, _registry, 10));

  AddSystem(std::make_shared<engine::ecs::systems::FiniteCleanerSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::FiniteSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::SequenceSystem>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::GrowPowerUp>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::ColorPowerUp>(_game, _registry));

  auto spawnSystem = std::make_shared<engine::ecs::systems::SpawnOverTime>(_game, _registry, _ballPrototype);
  spawnSystem->AddCondition([]() { return engine::Input::Instance().IsKeyPressed("create_ball"); });
  spawnSystem->SubscribeBeforeSpawn(&GenerateRandomVelocity);
  AddSystem(spawnSystem);
}

void SceneGame::InitEntts() {
  auto firstPlayer = _registry.create();
  _registry.emplace<engine::ecs::components::Component>(firstPlayer, "First Player");
  _registry.emplace<engine::ecs::components::Player>(firstPlayer, unsigned(1));
  _registry.emplace<engine::ecs::components::Player1>(firstPlayer);
  _registry.emplace<engine::ecs::components::Paddle>(firstPlayer);
  _registry.emplace<engine::ecs::components::Color>(firstPlayer);
  _registry.emplace<engine::ecs::components::Shadow2D>(firstPlayer);
  _registry.emplace<engine::ecs::components::Position>(firstPlayer, 25.0f, 25.0f);
  _registry.emplace<engine::ecs::components::Velocity>(firstPlayer, 0.0f, 0.0f);
  _registry.emplace<engine::ecs::components::Collider2D>(firstPlayer, 25.0f, 200.0f);
  _registry.emplace<engine::ecs::components::Size>(firstPlayer, 25.0f, 200.0f);

  auto secondPlayer = _registry.create();
  _registry.emplace<engine::ecs::components::Component>(secondPlayer, "Second Player");
  _registry.emplace<engine::ecs::components::Player>(secondPlayer, unsigned(2));
  _registry.emplace<engine::ecs::components::Player2>(secondPlayer);
  _registry.emplace<engine::ecs::components::Paddle>(secondPlayer);
  _registry.emplace<engine::ecs::components::Color>(secondPlayer);
  _registry.emplace<engine::ecs::components::AI>(secondPlayer);
  _registry.emplace<engine::ecs::components::Shadow2D>(secondPlayer);
  _registry.emplace<engine::ecs::components::Position>(secondPlayer, engine::Config::Instance()
                                                                       .GetSetting<unsigned int>("windowSizeX") -
                                                                     25.0f - 25.0f, 25.0f);
  _registry.emplace<engine::ecs::components::Velocity>(secondPlayer, 0.0f, 0.0f);
  _registry.emplace<engine::ecs::components::Collider2D>(secondPlayer, 25.0f, 200.0f);
  _registry.emplace<engine::ecs::components::Size>(secondPlayer, 25.0f, 200.0f);

  _ballPrototype = [](engine::Registry& registry) -> engine::Entity {
    auto ball = registry.create();
    registry.emplace<ecs::components::Ball>(ball);
    registry.emplace<engine::ecs::components::Component>(ball, "A Ball");
    registry.emplace<engine::ecs::components::Bounceable>(ball);
    registry.emplace<engine::ecs::components::Color>(ball);
    registry.emplace<engine::ecs::components::Shadow2D>(ball);
    registry.emplace<engine::ecs::components::Velocity>(ball, 1.0f, 1.0f);
    registry.emplace<engine::ecs::components::Position>(ball, 100.0f, engine::Config::Instance()
                                                                        .GetSetting<unsigned int>("windowSizeY") /
                                                                      2.0f);
    registry.emplace<engine::ecs::components::Collider2D>(ball, 25.0f, 25.0f);
    registry.emplace<engine::ecs::components::Size>(ball, 25.0f, 25.0f);
    return ball;
  };

  auto score = _game.GetRegistry().create();
  _game.GetRegistry().emplace<engine::ecs::components::Component>(score, std::string("Score"));
  _game.GetRegistry().emplace<engine::ecs::components::Score>(score, engine::ecs::components::Score{0, 0});


  int ballVelRelToRes = _game.GetConfig().values["game"]["ball"]["velocity"]["relativeToResolution"];
  auto ball1 = _ballPrototype(_registry);
  _registry.replace<engine::ecs::components::Velocity>(ball1, static_cast<float>(-ballVelRelToRes),
                                                       static_cast<float>(-ballVelRelToRes));
  auto ball2 = _ballPrototype(_registry);
  _registry
    .replace<engine::ecs::components::Position>(ball1, static_cast<float>(
                                                  engine::Config::Instance().GetSetting<unsigned int>("windowSizeX") - 100),
                                                static_cast<float>(
                                                  engine::Config::Instance().GetSetting<unsigned int>("windowSizeY") /
                                                  2));
  _registry.replace<engine::ecs::components::Velocity>(ball2, static_cast<float>(-ballVelRelToRes),
                                                       static_cast<float>(+ballVelRelToRes));
}

void SceneGame::ProcessInput(float dt) {
  // CHECK IF USER WANT TO GO TO THE MAIN MENU
  if (engine::Input::Instance().IsKeyPressed("menu")) {
    _game.SetCurrentScene("main_menu");
  }

  if (engine::Input::Instance().IsKeyPressed("power_up")) {
    _registry.view<engine::ecs::components::Player1>().each([this](auto entity) {
      auto growPowerUp = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(growPowerUp, "Grow PowerUp");
      _registry.emplace<engine::ecs::components::EntityTarget>(growPowerUp, entity);
      _registry.emplace<ecs::components::PowerUp>(growPowerUp, entity);
      _registry.emplace<ecs::components::SizePowerUp>(growPowerUp, 0.5f);
      _registry.emplace<engine::ecs::components::Finite>(growPowerUp, 1.0f);
    });
  }
  if (engine::Input::Instance().IsKeyPressed("grow")) {
    _registry.view<engine::ecs::components::Player1>().each([this](auto entity) {
      auto growPowerUp = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(growPowerUp, "Grow PowerUp");
      _registry.emplace<engine::ecs::components::EntityTarget>(growPowerUp, entity);
      _registry.emplace<ecs::components::PowerUp>(growPowerUp, entity);
      _registry.emplace<ecs::components::SizePowerUp>(growPowerUp, 1.2f);
      _registry.emplace<engine::ecs::components::Finite>(growPowerUp, 1.0f);
    });
  }
  if (engine::Input::Instance().IsKeyPressed("sequence")) {
    _registry.view<engine::ecs::components::Player1>().each([this](auto entity) {
      if (!_registry.view<ecs::components::ColorPowerUp>().empty()) return;

      auto growPowerUp = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(growPowerUp, "Grow PowerUp");
      _registry.emplace<engine::ecs::components::EntityTarget>(growPowerUp, entity);
      _registry.emplace<ecs::components::PowerUp>(growPowerUp, entity);
      _registry.emplace<ecs::components::SizePowerUp>(growPowerUp, 3.0f);
      _registry.emplace<engine::ecs::components::Finite>(growPowerUp, 3.0f, false);

      auto shrinkPowerUp = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(shrinkPowerUp, "Shrink PowerUp");
      _registry.emplace<engine::ecs::components::EntityTarget>(shrinkPowerUp, entity);
      _registry.emplace<ecs::components::PowerUp>(shrinkPowerUp, entity);
      _registry.emplace<ecs::components::SizePowerUp>(shrinkPowerUp, 0.3f);
      _registry.emplace<engine::ecs::components::Finite>(shrinkPowerUp, 3.0f, false);

      auto colorPowerUp = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(colorPowerUp, "Color Black PowerUp");
      _registry.emplace<engine::ecs::components::EntityTarget>(colorPowerUp, entity);
      _registry.emplace<ecs::components::PowerUp>(colorPowerUp, entity);
      _registry.emplace<ecs::components::ColorPowerUp>(colorPowerUp, engine::Color(136, 136, 136));
      _registry.emplace<engine::ecs::components::Finite>(colorPowerUp, 3.0f, false);

      auto seq = _registry.create();
      _registry.emplace<engine::ecs::components::Component>(seq, "Seq: [Shrink, Color, Grow]");
      _registry.emplace<engine::ecs::components::Finite>(seq, 100.0f, true);
      _registry.emplace<engine::ecs::components::Sequence>(
        seq,
        std::vector<engine::Entity>{shrinkPowerUp, colorPowerUp, growPowerUp});
    });
  }

  // CHECK KEYBOARD: PADDLE MOVEMENT FOR ALL PLAYERS
  _registry.view<engine::ecs::components::Player, engine::ecs::components::Position>()
           .each([&dt](auto entity, auto& player, auto& position) {
             auto moveByInPercent = engine::Config::Instance().paddleVelocityRelativeToRes;
             if (engine::Input::Instance().IsKeyPressed("up", player.index)) position.y -= moveByInPercent * dt;
             if (engine::Input::Instance().IsKeyPressed("down", player.index)) position.y += moveByInPercent * dt;
           });
}


}