#include <engine/Engine.hpp>
#include "scenes/SceneStart.hpp"
#include "scenes/SceneGame.hpp"
#include "scenes/GameOver.hpp"
#include <nlohmann/json.hpp>



void ProcessEvents(engine::Game& game) {
  Event event{};
  auto& window = game.GetWindow();
  if (Input::Instance().IsKeyPressed("quit")) window.close();
  while (window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) window.close();
  }
}

int main(int argc, const char **argv) {
  /*** Create Engine. It manages the main loop ***/
  engine::Engine engine;

  engine::GamePtr game = std::make_shared<engine::Game>();
  engine._onStepSubscription.connect([game](float) { ProcessEvents(*game); });
  engine.SetGame(game);
  engine.AddModules(std::make_shared<Module>());

  game->GetConfig().values["game"]["ball"]["spawn"]["position"]["player2"] = {{"x", 10}, {"y", 20}};
  game->GetConfig().values["game"]["ball"]["spawn"]["position"]["player1"] = {{"x", 10}, {"y", 20}};
  engine::ScenePtr mainMenuScene = std::make_shared<scenes::SceneStart>(*game);
  engine::ScenePtr gameScene = std::make_shared<scenes::SceneGame>(*game);
  engine::ScenePtr gameOver = std::make_shared<scenes::GameOver>(*game);
  game->AddScene(mainMenuScene);
  game->AddScene(gameScene);
  game->AddScene(gameOver);

  game->SetCurrentScene(mainMenuScene->GetName());
  engine.Run();
  return 0;
}
