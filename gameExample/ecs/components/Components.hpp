#pragma once

#include "Ball.hpp"
#include <engine/FrameworkIncludes.hpp>

namespace ecs {
namespace components {

struct PowerUp {
  engine::Entity entityId{0};
};

struct SizePowerUp {
  float mult = 1;
};

struct ColorPowerUp {
  engine::Color  colorNew = engine::Color::Black;
  engine::Color  colorOld = engine::Color::Black;
};

}
}