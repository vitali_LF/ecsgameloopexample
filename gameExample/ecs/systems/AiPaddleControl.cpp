#include "AiPaddleControl.hpp"
#include "../components/Ball.hpp"
#include <engine/ecs/components/Components.hpp>

namespace ecs::systems {

void AiPaddleControl::Update(float dt) {

  auto entityAIView = _registry.view<engine::ecs::components::AI>();
  entityAIView.each([this, dt](auto entityAI) {
    auto& positionPaddle = _registry.get<engine::ecs::components::Position>(entityAI);
    auto& velocityPaddle = _registry.get<engine::ecs::components::Velocity>(entityAI);
    auto& sizePaddle = _registry.get<engine::ecs::components::Size>(entityAI);
    auto middlePos = engine::Lerp(
      {positionPaddle.x, positionPaddle.y},
      {positionPaddle.x + sizePaddle.width, positionPaddle.y + sizePaddle.height},
      0.5f
    );


    // Find ball near the AI paddle
    auto entitiesBall = _registry.view<engine::ecs::components::Position, ecs::components::Ball>();
    auto nearBallEntt = static_cast<engine::Entity>(0);
    float minDistToPaddle = 1000000000.0f;
    entitiesBall.each([positionPaddle, &minDistToPaddle, &nearBallEntt](auto ballEntity, auto& ballPosition){
      auto distance = std::abs(ballPosition.x - positionPaddle.x);
      if (distance < minDistToPaddle) {
        minDistToPaddle = distance;
        nearBallEntt = ballEntity;
      }
    });

    auto moveByInPercent = engine::Config::Instance().paddleVelocityRelativeToRes;
    auto &ballPosition = _registry.get<engine::ecs::components::Position>(nearBallEntt);
    if (middlePos.y > ballPosition.y + 15) {
      // velocityPaddle.y -= moveByInPercent * dt;
      positionPaddle.y -= moveByInPercent * dt;
    } else if (middlePos.y < ballPosition.y - 15) {
      positionPaddle.y += moveByInPercent * dt;
    //   velocityPaddle.y += moveByInPercent * dt;
    }

  });

}
}