#pragma once

#include "AiPaddleControl.hpp"
#include "MissBall.hpp"
#include <engine/FrameworkIncludes.hpp>
#include "../components/Components.hpp"
#include <engine/ecs/components/Components.hpp>

namespace ecs::systems {

class PowerUpDetectionSystem : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override {
    auto collided = _registry.view<
      engine::ecs::components::HasCollidedTag,
      engine::ecs::components::Destructible,
      engine::ecs::components::Color>();
    for (auto&& item : collided) {
      auto& color = _registry.get<engine::ecs::components::Color>(item);
      auto& hasCollided = _registry.get<engine::ecs::components::HasCollidedTag>(item);
      LOG("BRICK: " + std::to_string(color.r) + " " + std::to_string(color.g) + " " + std::to_string(color.b));
      engine::Entity ballEntity = hasCollided.otherEntity;
      if (_registry.all_of<engine::ecs::components::LastCollisionByPlayer>(ballEntity)) {
        LOG("ITEM HAD COLLISION WITH A PLAYER OBJECT");
        auto playerEntt = _registry.get<engine::ecs::components::LastCollisionByPlayer>(ballEntity).playerEntity;
        if (color.r == 0 && color.g == 0 && color.b == 0) {
          LOG("BALL TOUCHED A BLACK BRICK");
          auto growPowerUp = _registry.create();
          _registry.emplace<engine::ecs::components::Component>(growPowerUp, "Grow PowerUp");
          _registry.emplace<engine::ecs::components::EntityTarget>(growPowerUp, playerEntt);
          _registry.emplace<ecs::components::PowerUp>(growPowerUp, playerEntt);
          _registry.emplace<ecs::components::SizePowerUp>(growPowerUp, 0.5f);
          _registry.emplace<engine::ecs::components::Finite>(growPowerUp, 3.0f);
        }
      }
    }
  }
};

class GameOver : public engine::System {
  int scoreToGameOver;

public:
  GameOver(engine::Game& game, engine::Registry& registry, int scoreToGameOver)
    : System(game, registry), scoreToGameOver(scoreToGameOver) {}

  void Update(float dt) override {
    _game.GetRegistry().view<engine::ecs::components::Score>().each([this](auto entity, auto& score) {
      if (score.player1 >= scoreToGameOver || score.player2 >= scoreToGameOver) {
        _game.SetCurrentScene("game_over");
      }
    });
  }
};

template<class PowerUp>
class PowerUpSystem : public engine::System {
public:
  using Entt = engine::Registry::entity_type;

  PowerUpSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {
    _registry.template on_construct<PowerUp>()
             .template connect<&PowerUpSystem::_PowerUpActivated>(this);
    _registry.template on_construct<PowerUp>()
             .template connect<&PowerUpSystem::_PowerUpDestroyed>(this);
  }

  void Update(float dt) override {
    _registry.view<engine::ecs::components::Finite, PowerUp>().each([&](auto entity, auto&& finite, auto&& pU) {
      if (!finite._started && finite._active) {
        finite._started = true;
        auto& entityTarget = _registry.get<engine::ecs::components::EntityTarget>(entity);
        PowerUpStarted(entity, entityTarget.entityId);

        auto& component = _registry.get<engine::ecs::components::Component>(entity);
        LOG("PowerUpStarted: " << component.name);
      }
      if (finite._finished) {
        auto& entityTarget = _registry.get<engine::ecs::components::EntityTarget>(entity);
        PowerUpFinished(entity, entityTarget.entityId);

        auto& component = _registry.get<engine::ecs::components::Component>(entity);
        LOG("PowerUpFinished: " << component.name);
      }
    });
  }

  virtual void PowerUpConstructed(Entt entity) {};

  virtual void PowerUpDestroyed(Entt entity) {};

  virtual void PowerUpStarted(Entt entity, Entt target) {};

  virtual void PowerUpFinished(Entt entity, Entt target) {};

private:
  void _PowerUpActivated(engine::Registry& registry, engine::Entity entity) {
    PowerUpConstructed(entity);
  };

  void _PowerUpDestroyed(engine::Registry& registry, engine::Entity entity) {
    PowerUpDestroyed(entity);
  };
};

class GrowPowerUp : public ecs::systems::PowerUpSystem<ecs::components::SizePowerUp> {
public:
  using ecs::systems::PowerUpSystem<components::SizePowerUp>::PowerUpSystem;

  void PowerUpStarted(Entt entity, Entt target) override {
    auto& entityTarget = _registry.get<engine::ecs::components::EntityTarget>(entity);
    auto& size = _registry.get<engine::ecs::components::Size>(entityTarget.entityId);
    auto& coll = _registry.get<engine::ecs::components::Collider2D>(entityTarget.entityId);
    auto& sizePowerUp = _registry.get<components::SizePowerUp>(entity);
    size.height *= sizePowerUp.mult;
    coll.height *= sizePowerUp.mult;
  }

  void PowerUpFinished(Entt entity, Entt target) override {
    auto& sizePowerUp = _registry.get<components::SizePowerUp>(entity);
    auto& size = _registry.get<engine::ecs::components::Size>(target);
    auto& coll = _registry.get<engine::ecs::components::Collider2D>(target);
    _registry.emplace_or_replace<engine::ecs::components::Size>(target, size.width, size.height / sizePowerUp.mult);
    _registry.emplace_or_replace<engine::ecs::components::Collider2D>(target, coll.width, coll.height / sizePowerUp.mult);
  }
};

class ColorPowerUp : public ecs::systems::PowerUpSystem<components::ColorPowerUp> {
public:
  using ecs::systems::PowerUpSystem<components::ColorPowerUp>::PowerUpSystem;

  void PowerUpStarted(Entt entity, Entt target) override {
    auto&& color = _registry.get<engine::ecs::components::Color>(target);
    auto&& sizePowerUp = _registry.get<components::ColorPowerUp>(entity);
    sizePowerUp.colorOld = color;
    color = sizePowerUp.colorNew;
  }

  void PowerUpFinished(Entt entity, Entt target) override {
    auto& colorPowerUp = _registry.get<components::ColorPowerUp>(entity);
    _registry.emplace_or_replace<engine::ecs::components::Color>(target, colorPowerUp.colorOld);
  }
};

}
