#pragma once

#include <engine/System.hpp>

namespace ecs::systems {

class MissBallSystem : public engine::System {

public:
  using engine::System::System;

  void Update(float dt) override;
};

}
