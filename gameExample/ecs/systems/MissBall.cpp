#include "MissBall.hpp"
#include "../components/Components.hpp"
#include <engine/ecs/components/Components.hpp>

namespace ecs::systems {
namespace component = engine::ecs::components;
namespace componentGame = ecs::components;

void MissBallSystem::Update(float dt) {
  auto width = engine::Config::Instance().GetSetting<unsigned int>("windowSizeX");
  auto height = engine::Config::Instance().GetSetting<unsigned int>("windowSizeY");
  auto view = _registry.view<componentGame::Ball, component::Position, component::Collider2D, component::Velocity>();
  for (auto entity: view) {
    auto& position = view.get<component::Position>(entity);
    auto& coll = view.get<component::Collider2D>(entity);
    auto& velocity = view.get<component::Velocity>(entity);


    auto ballVelRelToRes = engine::Config::Instance().ballVelocityRelativeToRes;
    auto scoreView = _game.GetRegistry().view<component::Score>();
    scoreView.each([&](auto entity, auto& score) {
      auto damageEntt = _registry.create();
      auto shakeCamEntt = _registry.create();

      if (position.x + coll.width >= width) {
        _registry.emplace<component::Damage>(damageEntt, engine::Entity(2));
        _registry.emplace<component::ShakeCamera>(shakeCamEntt, 0.4f, component::ShakeCamera::Right);
        score.player1++;
        velocity.x = -ballVelRelToRes;
        velocity.y = -ballVelRelToRes;
        position.x = engine::Config::Instance().GetSetting<unsigned int>("windowSizeX") - 100.0f;
        position.y = engine::Config::Instance().GetSetting<unsigned int>("windowSizeY") / 2.0f;
      } else if (position.x <= 0) {
        _registry.emplace<component::Damage>(damageEntt, engine::Entity(1));
        _registry.emplace<component::ShakeCamera>(shakeCamEntt, 0.4f, component::ShakeCamera::Left);
        score.player2++;
        position.x = 100;
        position.y = engine::Config::Instance().GetSetting<unsigned int>("windowSizeY") / 2.0f;
      }
    });
  }
}
}