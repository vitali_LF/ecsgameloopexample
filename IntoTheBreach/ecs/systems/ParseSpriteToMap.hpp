#pragma once

#include <engine/System.hpp>

namespace ecs::systems {

class ParseSpriteToMap : public engine::System {
public:
  ParseSpriteToMap(engine::Game& game, engine::Registry& registry, std::string spritePath);

  void Update(float dt) override;

private:
  std::string _spritePath;
  entt::resource<engine::Texture> _textureHandle;
};

class SizePositionBasedOnTileIndex : public engine::System {
public:
  SizePositionBasedOnTileIndex(engine::Game& game, engine::Registry& registry);

  void Update(float dt) override;

};

}

