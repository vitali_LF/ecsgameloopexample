#pragma once

#include <engine/FrameworkIncludes.hpp>
#include <engine/ecs/components/Components.hpp>
#include <components/Components.hpp>

namespace ecs::systems {

class MousePositionSystem : public engine::System {
public:
  using engine::System::System;

  MousePositionSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {}

  void Update(float dt) override;
};

class MouseHoverSystem : public engine::System {
public:
  using engine::System::System;

  MouseHoverSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {}

  void Update(float dt) override;
};

class SelectionSystem : public engine::System {
public:
  using engine::System::System;

  SelectionSystem(engine::Game& game, engine::Registry& registry);

  void Update(float dt) override;
};

class ShowMovementRangeSystem : public engine::System {
public:
  using engine::System::System;

  ShowMovementRangeSystem(engine::Game& game, engine::Registry& registry);

  void Update(float dt) override;
};

}