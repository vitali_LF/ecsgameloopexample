#include "RenderTiles.hpp"
#include "../components/Components.hpp"
#include <engine/ecs/components/Components.hpp>
#include <engine/core_system/Resources.hpp>

namespace ecs::systems {

void ComputeTileSize(engine::Game&, float dt) {
  auto tileSize = engine::Config::Instance().GetSetting<float>("displayHeight1Percent") * 12;
  auto tilePadding = engine::Config::Instance().GetSetting<float>("displayHeight1Percent") * 1 / 4;
  engine::Config::Instance().SetSetting<float>("MapTileSize", tileSize);
  engine::Config::Instance().SetSetting<float>("MapTilePadding", tilePadding);
}

RenderTiles::RenderTiles(engine::Game& game, engine::Registry& registry)
  : System(game, registry) {}

void RenderTiles::Update(float dt) {
  auto tileSize = engine::Config::Instance().GetSetting<float>("MapTileSize");
  auto tilePadding = engine::Config::Instance().GetSetting<float>("MapTilePadding");
  auto startX = tilePadding * 8;
  auto startY = tilePadding * 1;

  {
    sf::RectangleShape r;

    _registry
      .view<engine::ecs::components::Index, engine::ecs::components::Color>(
        entt::exclude<engine::ecs::components::TextureComponent>)
      .each([&](auto entity, auto& index, auto& color) {


        float positionX = startX + tileSize * index.x + index.x * tilePadding;
        float positionY = startY + tileSize * index.y + index.y * tilePadding;


        r.setSize({(float) tileSize, (float) tileSize});
        r.setPosition({(float) positionX, (float) positionY});
        r.setFillColor(color);
        _game.GetWindow().draw(r);
      });
  }
  {
    sf::RectangleShape r;

    _registry
      .view<engine::ecs::components::Index, engine::ecs::components::TextureComponent>()
      .each([&](auto entity, auto& index, auto& textureComponent) {


        float positionX = startX + tileSize * index.x + index.x * tilePadding;
        float positionY = startY + tileSize * index.y + index.y * tilePadding;

        _registry.emplace_or_replace<engine::ecs::components::Collider2D>(entity, tileSize, tileSize);


        r.setSize({(float) tileSize, (float) tileSize});
        r.setPosition({(float) positionX, (float) positionY});
        r.setTexture(engine::Resources::Instance().GetTexture(textureComponent.texturePath).handle().get());
        _game.GetWindow().draw(r);
      });
  }
}

}