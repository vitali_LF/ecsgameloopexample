#pragma once

#include <engine/System.hpp>

namespace ecs::systems {

void ComputeTileSize(engine::Game&, float dt);

class RenderTiles : public engine::System {
public:
  RenderTiles(engine::Game& game, engine::Registry& registry);

  void Update(float dt) override;
};

}

