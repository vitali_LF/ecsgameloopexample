#include "ParseSpriteToMap.hpp"
#include <components/Components.hpp>
#include <components/MapTile.hpp>
#include <engine/ecs/components/Components.hpp>
#include <engine/core_system/Resources.hpp>
#include <fstream>



namespace ecs::systems {

SizePositionBasedOnTileIndex::SizePositionBasedOnTileIndex(engine::Game& game, engine::Registry& registry)
  : System(game, registry) {
}


void SizePositionBasedOnTileIndex::Update(float dt) {
  auto tileSize = engine::Config::Instance().GetSetting<float>("MapTileSize");
  auto tilePadding = engine::Config::Instance().GetSetting<float>("MapTilePadding");
  auto startX = tilePadding * 3;
  auto startY = tilePadding * 3;

  _registry
    .view<engine::ecs::components::Position,
      engine::ecs::components::Size,
      engine::ecs::components::Collider2D,
      engine::ecs::components::Index>().each([&](auto entity, auto& pos, auto& size, auto& collider, auto& index) {
    float positionX = startX + tileSize * index.x + index.x * tilePadding;
    float positionY = startY + tileSize * index.y + index.y * tilePadding;
    _registry.replace<engine::ecs::components::Position>(entity, positionX, positionY);
    _registry.replace<engine::ecs::components::Collider2D>(entity, tileSize, tileSize);
    _registry.replace<engine::ecs::components::Size>(entity, tileSize, tileSize);
  });
}


ParseSpriteToMap::ParseSpriteToMap(engine::Game& game, engine::Registry& registry, std::string spritePath)
  : System(game, registry), _spritePath(std::move(spritePath)), _textureHandle(
  engine::Resources::Instance().GetTexture(_spritePath)) {



  std::ifstream tilesFileStream("tiles.json");
  engine::Config::Instance().values["tiles"] = nlohmann::json::parse(tilesFileStream);
  tilesFileStream.close();
  //
  //
  // auto colorStone = engine::Color(195, 195, 195);
  // auto colorTree = engine::Color(107, 153, 89);
  // auto colorGrass = engine::Color(141, 168, 95);
  // auto colorWater = engine::Color(104, 140, 155);
  //
  // engine::Config::Instance().values["tiles"] = {
  //   {
  //     "stone", {
  //                {"id", std::uint64_t(0)},
  //                {"sourceColor", colorStone},
  //                {"path", "resources/stone2.png"},
  //                {"canWalkOn", true}
  //              }
  //   },
  //   {
  //     "tree",  {
  //                {"id", std::uint64_t(1)},
  //                {"sourceColor", colorTree},
  //                {"path", "resources/tree.png"},
  //                {"canWalkOn", false}
  //              }
  //   },
  //   {
  //     "grass", {
  //                {"id", std::uint64_t(2)},
  //                {"sourceColor", colorGrass},
  //                {"path", "resources/grass1.png"},
  //                {"canWalkOn", true}
  //              }
  //   },
  //   {
  //     "water", {
  //                {"id", std::uint64_t(3)},
  //                {"sourceColor", colorWater},
  //                {"path", "resources/water.png"},
  //                {"canWalkOn", false}
  //              }
  //   }
  // };


  // std::ofstream myfile;
  // myfile.open ("tiles.json");
  // myfile << engine::Config::Instance().values["tiles"].dump(4);
  // myfile.close();
}


void ParseSpriteToMap::Update(float dt) {
  auto image = _textureHandle->copyToImage();
  auto tiles = engine::Config::Instance().values["tiles"].get<std::unordered_map<std::string, components::MapTile>>();

  for (unsigned int y = 0; y < image.getSize().y; y++) {
    for (unsigned int x = 0; x < image.getSize().x; x++) {
      auto pixel = image.getPixel(x,y);

      auto entity = _registry.create();

      _registry.emplace<engine::ecs::components::Component>(entity, "MapTile2D");
      _registry.emplace<engine::ecs::components::Color>(entity, pixel.r, pixel.g, pixel.b, pixel.a);
      _registry.emplace<engine::ecs::components::Shadow2D>(entity);
      _registry.emplace<engine::ecs::components::Position>(entity, 0.0f, 0.0f);
      _registry.emplace<engine::ecs::components::Collider2D>(entity, 0.0f, 0.0f);
      _registry.emplace<engine::ecs::components::Size>(entity, 0.0f, 0.0f);
      _registry.emplace<engine::ecs::components::Index>(entity, x, y);
      _registry.emplace<engine::ecs::components::SizeInPercent>(entity, 8.0f, 8.0f);
      _registry.emplace<components::ClickableShape>(entity);
      _registry.emplace<components::Selectable>(entity);

      auto tile = components::findPathByColor(tiles, pixel);
      if (tile == tiles.end()) {
        continue;
      }
      _registry.emplace<engine::ecs::components::TextureComponent>(entity, tile->second.path);
      _registry.emplace<components::MapTile>(entity, tile->second);
    }
  }
  System::SetFinished(true);
}

}