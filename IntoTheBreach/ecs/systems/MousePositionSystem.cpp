#include "MousePositionSystem.hpp"
#include <SFML/Window.hpp>
#include <components/Components.hpp>
#include <components/MapTile.hpp>

namespace ecs::systems {

engine::Vector2 PositionComponentToVector2(const engine::ecs::components::Position& position) {
  return {
    position.x,
    position.y
  };
}

engine::Vector2 SizeComponentToVector2(const engine::ecs::components::Collider2D& position) {
  return {
    position.width,
    position.height
  };
}

void MousePositionSystem::Update(float dt) {

  auto mousePos = sf::Mouse::getPosition(_game.GetWindow());
  _registry.view<engine::ecs::components::Collider2D, engine::ecs::components::Position, components::ClickableShape>()
           .each([&mousePos](auto entity, auto& box2DCollider, auto& position, auto& clickableShape) {
             auto rect = RectWrapper(PositionComponentToVector2(position), SizeComponentToVector2(box2DCollider));
             if (rect.ContainsPosition(mousePos)) {
               clickableShape.containsMouse = true;
               clickableShape.isClicked = sf::Mouse::isButtonPressed(sf::Mouse::Left);
             } else {
               clickableShape.containsMouse = false;
               clickableShape.isClicked = false;
             }
           });
}

void MouseHoverSystem::Update(float dt) {
  sf::RectangleShape r;
  _registry.view<engine::ecs::components::Position, engine::ecs::components::Collider2D, components::ClickableShape>()
           .each([&](auto entity, auto& pos, auto& size, auto& clickableShape) {
             if (!clickableShape.containsMouse) {
               return;
             }
             r.setSize({(float) size.width, (float) size.height});
             r.setPosition({(float) pos.x, (float) pos.y});
             r.setOutlineColor(engine::Color(240, 237, 100, 190));
             r.setFillColor(sf::Color::Transparent);
             r.setOutlineThickness(10.0f);
             _game.GetWindow().draw(r);
           });
}

SelectionSystem::SelectionSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {
  _registry.ctx().emplace<components::Selection>(
    engine::ecs::components::Index{size_t(0), size_t(0)},
    engine::ecs::components::Index{size_t(0), size_t(0)},
    entt::null,
    entt::null
  );
}

void SelectionSystem::Update(float dt) {
  // select one of the player character
  auto& selection = _registry.ctx().get<components::Selection>();
  _registry.view<components::ClickableShape, components::Selectable, engine::ecs::components::Index, components::PlayerCharacter>()
           .each([&](auto entity, auto& clickableShape, auto& selectable, auto& index, auto& player) {
             if (clickableShape.containsMouse && clickableShape.isClicked) {
               selection.selectedCharacterIndex = index;
               selection.entityCharacter = entity;
               selection.selectedTileMapIndex = engine::ecs::components::Index::Zero();
               selection.entityMapTile = entt::null;
             }
           });

  // select one of the map tiles
  _registry.view<components::ClickableShape, components::Selectable, engine::ecs::components::Index, components::MapTile>()
           .each([&](auto entity, auto& clickableShape, auto& selectable, auto& index, auto& mapTile) {
             if (clickableShape.containsMouse && clickableShape.isClicked & index != selection.selectedCharacterIndex) {
               selection.selectedTileMapIndex = index;
               selection.entityMapTile = entity;
             }
           });


  sf::RectangleShape r;
  // Draw red border around selected char
  _registry.view<engine::ecs::components::Position, engine::ecs::components::Collider2D,
             engine::ecs::components::Index,
             components::Selectable, components::PlayerCharacter>()
           .each([&](auto entity, auto& pos, auto& collider, auto& index, auto& selectable, auto& character) {
             if (index != selection.selectedCharacterIndex) {
               return;
             }
             r.setSize({(float) collider.width, (float) collider.height});
             r.setPosition({(float) pos.x, (float) pos.y});
             r.setOutlineColor(engine::Color(240, 100, 100, 190));
             r.setFillColor(sf::Color::Transparent);
             r.setOutlineThickness(10.0f);
             _game.GetWindow().draw(r);
           });

  if (selection.entityMapTile != entt::null) {
    auto selectedTilePosition = _registry.get<engine::ecs::components::Position>(selection.entityMapTile);
    auto selectedTileCollider = _registry.get<engine::ecs::components::Collider2D>(selection.entityMapTile);
    r.setSize({(float) selectedTileCollider.width, (float) selectedTileCollider.height});
    r.setPosition({(float) selectedTilePosition.x, (float) selectedTilePosition.y});
    r.setOutlineColor(engine::Color(240, 237, 100, 190));
    r.setFillColor(sf::Color::Transparent);
    r.setOutlineThickness(10.0f);
    _game.GetWindow().draw(r);
  }
}


ShowMovementRangeSystem::ShowMovementRangeSystem(engine::Game& game, engine::Registry& registry)
  : System(game, registry) {}

void ShowMovementRangeSystem::Update(float dt) {
  auto& selection = _registry.ctx().get<components::Selection>();
  sf::RectangleShape movementRangeRect;
  _registry
    .view<
      engine::ecs::components::Position,
      engine::ecs::components::Collider2D,
      components::ClickableShape,
      components::MapTile,
      engine::ecs::components::Index>()
    .each([&](auto entity, auto& pos, auto& size, auto& clickableShape, auto& mapTile, auto& index) {

      if (selection.selectedCharacterIndex == index || selection.entityCharacter == entt::null) {
        return;
      }

      if (!mapTile.canWalkOn) {
        return;
      }


      auto character = _registry.get<components::PlayerCharacter>(selection.entityCharacter);
      auto right = static_cast<int>(selection.selectedCharacterIndex.x) + character.movementRange;
      auto left = static_cast<int>(selection.selectedCharacterIndex.x) - character.movementRange;
      auto top = static_cast<int>(selection.selectedCharacterIndex.y) + character.movementRange;
      auto bottom = static_cast<int>(selection.selectedCharacterIndex.y) - character.movementRange;
      int indexX = index.x;
      int indexY = index.y;

      auto getManhattanDistance = [&]() {
        auto lengthX = indexX - static_cast<int>(selection.selectedCharacterIndex.x);
        auto lengthY = indexY - static_cast<int>(selection.selectedCharacterIndex.y);
        return std::abs(lengthX) + std::abs(lengthY);
      };

      if (getManhattanDistance() > character.movementRange) {
        return;
      }

      if (indexX <= right &&
          indexX >= left &&
          indexY <= top &&
          indexY >= bottom) {
        movementRangeRect.setSize({(float) size.width, (float) size.height});
        movementRangeRect.setPosition({(float) pos.x, (float) pos.y});
        movementRangeRect.setFillColor(engine::Color(71, 220, 50, 100));
        movementRangeRect.setOutlineColor(engine::Color(71, 220, 50, 130));
        movementRangeRect.setOutlineThickness(5.0f);
        _game.GetWindow().draw(movementRangeRect);
      }
    });
}

}