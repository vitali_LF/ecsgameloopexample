#pragma once

#include <engine/FrameworkIncludes.hpp>
#include <engine/ecs/components/Components.hpp>
#include "../components/Components.hpp"

namespace ecs::systems {

class PowerUpDetectionSystem : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override {
    auto collided = _registry.view<
      engine::ecs::components::HasCollidedTag,
      engine::ecs::components::Destructible,
      engine::ecs::components::Color>();
    for (auto&& item : collided) {
      auto& color = _registry.get<engine::ecs::components::Color>(item);
      auto& hasCollided = _registry.get<engine::ecs::components::HasCollidedTag>(item);
      LOG("BRICK: " + std::to_string(color.r) + " " + std::to_string(color.g) + " " + std::to_string(color.b));
      engine::Entity ballEntity = hasCollided.otherEntity;
      if (_registry.all_of<engine::ecs::components::LastCollisionByPlayer>(ballEntity)) {
        LOG("ITEM HAD COLLISION WITH A PLAYER OBJECT");
        auto playerEntt = _registry.get<engine::ecs::components::LastCollisionByPlayer>(ballEntity).playerEntity;
        if (color.r == 0 && color.g == 0 && color.b == 0) {
          LOG("BALL TOUCHED A BLACK BRICK");
          auto growPowerUp = _registry.create();
          _registry.emplace<engine::ecs::components::Component>(growPowerUp, "Grow PowerUp");
          _registry.emplace<engine::ecs::components::EntityTarget>(growPowerUp, playerEntt);
          _registry.emplace<engine::ecs::components::Finite>(growPowerUp, 3.0f);
        }
      }
    }
  }
};

class PowerUpSystem : public engine::System {
public:
  using Entt = engine::Registry::entity_type;

  PowerUpSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {
    // _registry.template on_construct<PowerUp>()
    //          .template connect<&PowerUpSystem::_PowerUpActivated>(this);
    // _registry.template on_construct<PowerUp>()
    //          .template connect<&PowerUpSystem::_PowerUpDestroyed>(this);
  }

  void Update(float dt) override {
    // _registry.view<engine::ecs::components::Finite, PowerUp>().each([&](auto entity, auto&& finite, auto&& pU) {});
  }
};

}
