#pragma once

#include <engine/FrameworkIncludes.hpp>

namespace ecs::components {

namespace sf {
class Shape;
}


struct Velocity {
  float dx;
  float dy;
};

struct ClickableShape {
  bool containsMouse;
  bool isClicked;
};

struct Selectable {
  bool selected = false;
};

struct Selection {
  engine::ecs::components::Index selectedCharacterIndex;
  engine::ecs::components::Index selectedTileMapIndex;
  engine::Entity entityMapTile;
  engine::Entity entityCharacter;
};

struct PlayerCharacter {
  std::string type;
  std::string texturePath;
  int movementRange;
};

struct DrawableShape {
  std::shared_ptr<engine::Shape> shape = nullptr;
  engine::Color color = engine::Color::White;
  int z = 0;
};

struct SpriteComponent {
  std::shared_ptr<engine::Sprite> sprite;
  int z = 0;
};

struct Name {
  std::string value;
};

struct Texture2D {
  std::shared_ptr<engine::Texture> texture;
  engine::RectangleShape shape;
};

struct CoinFound {
  bool found = true;
};

struct GameOverScreen {
};
struct Enemy {
};
struct MainCharacter {
};

}