#include "MapTile.hpp"

namespace sf {
void to_json(nlohmann::json& j, const Color& p) {
  j = nlohmann::json{{"r", p.r},
                     {"g", p.g},
                     {"b", p.b},
                     {"a", p.a}};
}

void from_json(const nlohmann::json& j, Color& p) {
  j.at("r").get_to(p.r);
  j.at("g").get_to(p.g);
  j.at("b").get_to(p.b);
  j.at("a").get_to(p.a);
}
}

namespace ecs::components {

void to_json(nlohmann::json& j, const MapTile& p) {
  j = nlohmann::json{{"id",          p.id},
                     {"sourceColor", p.sourceColor},
                     {"path",        p.path},
                     {"canWalkOn",   p.canWalkOn}};
}

void from_json(const nlohmann::json& j, MapTile& p) {
  j.at("id").get_to(p.id);
  j.at("sourceColor").get_to(p.sourceColor);
  j.at("path").get_to(p.path);
  j.at("canWalkOn").get_to(p.canWalkOn);
}

auto findPathByColor(const MapTilesType& map, const engine::Color& color) -> MapTilesType::const_iterator {
  return std::find_if(std::begin(map), std::end(map),
                      [&color](const std::pair<std::string, MapTile>& pair) {
                        return color == pair.second.sourceColor;
                      });
}

}