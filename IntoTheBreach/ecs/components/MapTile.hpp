#pragma once

#include <engine/FrameworkIncludes.hpp>

namespace sf {
void to_json(nlohmann::json& j, const Color& p);
void from_json(const nlohmann::json& j, Color& p);
}

namespace ecs::components {

struct MapTile {
  std::uint64_t id;
  engine::Color sourceColor;
  std::string path;
  bool canWalkOn;
};

void to_json(nlohmann::json& j, const MapTile& p);
void from_json(const nlohmann::json& j, MapTile& p);

// example
// std::transform(tiles.begin(), tiles.end(), std::inserter(tileTextures, tileTextures.end()), components::convertToColorToPath);
auto convertToColorToPath = [](std::pair<const std::string, components::MapTile>& pair)
  -> std::pair<const engine::Color, std::string> {
  return {pair.second.sourceColor, pair.second.path};
};

using MapTilesType = std::unordered_map<std::string, components::MapTile>;

auto findPathByColor(const MapTilesType& map, const engine::Color& color) -> MapTilesType::const_iterator;

}