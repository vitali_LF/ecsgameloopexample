#include "SceneGame.hpp"
#include "systems/MousePositionSystem.hpp"
#include <components/Components.hpp>
#include <systems/Systems.hpp>
#include <systems/ParseSpriteToMap.hpp>
#include <systems/RenderTiles.hpp>
#include <engine/core_system/Input.hpp>
#include <engine/ecs/systems/CubeArray.hpp>
#include <engine/FrameworkIncludes.hpp>

namespace scenes {

SceneGame::SceneGame(engine::Game& game) : engine::Scene(game, "game") {
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundR", 2);
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundG", 26);
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundB", 51);
  engine::Config::Instance().SetSetting<unsigned int>("windowSizeX", 730);
  engine::Config::Instance().SetSetting<unsigned int>("windowSizeY", 720);
  _soundStart = std::make_shared<engine::Sound>("sounds/start.wav");
}

void CreateBoxIso3D(size_t indexX, size_t indexY) {

  // for (size_t j = 0; j < 10; ++j) {
  //   for (size_t i = 0; i < 10; ++i) {
  //     auto cube = _registry.create();
  //     engine::ecs::components::Size size{30.0f, 30.0f};
  //     _registry.emplace<engine::ecs::components::Component>(cube, "Iso 3D Cube ");
  //     _registry.emplace<engine::ecs::components::BoxIso3D>(cube);
  //     _registry.emplace<engine::ecs::components::Color>(cube);
  //     _registry.emplace<engine::ecs::components::Index>(cube, j, i);
  //     _registry.emplace<engine::ecs::components::Position>(cube, 100.0f + (size.width * i * 2),
  //                                                          100.0f + (size.height * j * 2));
  //     _registry.emplace<engine::ecs::components::Size>(cube, size);
  //     _registry.emplace<engine::ecs::components::Offset>(cube, 0.0f * i, 0.0f * j);
  //   }
  // }
  // return cube;
}

void SceneGame::Update(float dt) {
  if (!_running) return;
  engine::Scene::Update(dt);
  ProcessInput(dt);
}

void SceneGame::Start() {
  LOG("Start Game");
  Scene::Start();
  _soundStart->Play();
}

void SceneGame::Init() {
  LOG("Init Game");
  ecs::systems::ComputeTileSize(_game, 0.0f);
  InitEntts();
  InitSystems();
}

void SceneGame::Finish() {
  std::cout << "Finish Game" << '\n';
}

void SceneGame::InitSystems() {
  AddSystemLambda(&ecs::systems::ComputeTileSize);
  AddSystem(std::make_shared<ecs::systems::ParseSpriteToMap>(_game, _registry, "resources/map.png"));
  AddSystem(std::make_shared<ecs::systems::SizePositionBasedOnTileIndex>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::Render>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::CubeArray>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::AnimationStarterSystem>(_game, _registry, *this));
  AddSystem(std::make_shared<engine::ecs::systems::CameraRotationSystem>(_game, _registry, *this));
  AddSystem(std::make_shared<engine::ecs::systems::Score>(_game, _registry));

  AddSystem(std::make_shared<engine::ecs::systems::FiniteCleanerSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::FiniteSystem>(_game, _registry));
  AddSystem(std::make_shared<engine::ecs::systems::SequenceSystem>(_game, _registry));

  AddSystem(std::make_shared<ecs::systems::MousePositionSystem>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::MouseHoverSystem>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::SelectionSystem>(_game, _registry));
  AddSystem(std::make_shared<ecs::systems::ShowMovementRangeSystem>(_game, _registry));
}
// struct ecs::components::PlayerCharacter {
//
// };

engine::Entity CreatePlayerCharacter(engine::Registry& registry, const std::string& type, engine::ecs::components::Index index) {
  static std::unordered_map<std::string, ecs::components::PlayerCharacter> characters{
    {"pers1", {"pers1", "resources/Mech_electric.png", 3}},
    {"pers2", {"pers2", "resources/Mech_2.png",        2}}
  };

  auto merchEntity = registry.create();

  auto pers = characters.find(type);
  if (pers == characters.end()) {
    ERROR("Character " << type << "not found");
    return entt::null;
  }
  registry.emplace<engine::ecs::components::Component>(merchEntity, "Character");
  registry.emplace<engine::ecs::components::Color>(merchEntity, 100, 100, 100);
  registry.emplace<engine::ecs::components::TextureComponent>(merchEntity, pers->second.texturePath);
  registry.emplace<engine::ecs::components::Shadow2D>(merchEntity);
  registry.emplace<engine::ecs::components::Index>(merchEntity, index);
  registry.emplace<engine::ecs::components::Position>(merchEntity, 0.0f, 0.0f);
  registry.emplace<engine::ecs::components::Collider2D>(merchEntity, 0.0f, 0.0f);
  registry.emplace<engine::ecs::components::Size>(merchEntity, 0.0f, 0.0f);
  registry.emplace<ecs::components::PlayerCharacter>(merchEntity, pers->second);
  registry.emplace<ecs::components::ClickableShape>(merchEntity);
  registry.emplace<ecs::components::Selectable>(merchEntity);
  return merchEntity;
}

void SceneGame::InitEntts() {
  CreatePlayerCharacter(_registry, "pers1", engine::ecs::components::Index{size_t(2), size_t(2)});
  CreatePlayerCharacter(_registry, "pers2", engine::ecs::components::Index{size_t(5), size_t(1)});

  auto score = _game.GetRegistry().create();
  _game.GetRegistry().emplace<engine::ecs::components::Component>(score, std::string("Score"));
  _game.GetRegistry().emplace<engine::ecs::components::Score>(score, engine::ecs::components::Score{0, 0});
}

void SceneGame::ProcessInput(float dt) {
  // CHECK IF USER WANT TO GO TO THE MAIN MENU
  if (engine::Input::Instance().IsKeyPressed("menu")) {
    _game.SetCurrentScene("main_menu");
  }
}

}