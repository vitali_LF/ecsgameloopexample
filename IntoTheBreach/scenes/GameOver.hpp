#ifndef ECSGAMELOOP_SCENE_GAMEOVER_HPP
#define ECSGAMELOOP_SCENE_GAMEOVER_HPP


#include <engine/Scene.hpp>
#include <engine/FrameworkIncludes.hpp>

using namespace engine;

namespace scenes {

class GameOver : public engine::Scene {

public:
  explicit GameOver(engine::Game& game) : Scene(game, "game_over") {};

  void Init() override {
    font.loadFromFile(Resources::Instance().GetPath("fonts/open-sans/OpenSans-Regular.ttf"));

    text.setString("Game Over");
    text.setFont(font);
    text.setCharacterSize(32);
    text.setFillColor(engine::Color::White);
    text.setPosition((float)_game.GetConfig().GetSetting<float>("displayWidth1Percent") * 25,
                     (float)_game.GetConfig().GetSetting<float>("displayHeight1Percent") * 50);
  }

  void Update(float s) override {
    _game.GetWindow().draw(text);
  }
  engine::Text text;
  engine::Font font;
};

}

#endif //ECSGAMELOOP_SCENE_GAMEOVER_HPP
