#ifndef ECSGAMELOOP_SCENESTART_HPP
#define ECSGAMELOOP_SCENESTART_HPP


#include <engine/Scene.hpp>
#include <engine/core_system/Resources.hpp>
#include <engine/core_system/Input.hpp>
#include <engine/ecs/components/Components.hpp>
#include <engine/ecs/systems/Systems.hpp>
#include <utility>

using namespace engine::ecs;
using namespace engine;

constexpr HashedString backgroundPath {"resources/Background.png"};
constexpr HashedString startButtonPath {"resources/Button_Start.png"};
constexpr HashedString startButtonSelectedPath {"resources/Button_Start_white.png"};
constexpr HashedString exitButtonPath {"resources/Button_EXIT.png"};
constexpr HashedString exitButtonSelectedPath {"resources/Button_EXIT_white.png"};

static const char* const SCENE_NAME = "main_menu";

struct Button {
  const HashedString* normalPath;
  const HashedString* selectedPath;
};

struct Action {
  std::string label;
  std::function<void(void)> function;
};

struct Menu {
  int selectedIndex;
  std::vector<unsigned int> entities;
};

namespace scenes {
class SceneStart : public engine::Scene {

  void ButtonSystem() {
    _registry.view<Menu>().each([this](auto entity, auto& menu){
      int changedIndex = -1;
      if (Input::Instance().IsKeyPressed("left")) {
        if (menu.selectedIndex > 0) {
          changedIndex = menu.selectedIndex-1;
        }
      }
      if (Input::Instance().IsKeyPressed("right")) {
        if (menu.selectedIndex < menu.entities.size() -1) {
          changedIndex = menu.selectedIndex+1;
        }
      }

      if (changedIndex == -1) return;
      auto& currentSelectionButton = _registry.get<Button>(
        static_cast<const entt::entity>(menu.entities[menu.selectedIndex]));
      auto& currentSelectionTexture = _registry.get<components::Textureable>(
        static_cast<const entt::entity>(menu.entities[menu.selectedIndex]));
      auto& nextSelectionButton = _registry.get<Button>(static_cast<const entt::entity>(menu.entities[changedIndex]));
      auto& nextSelectionTexture = _registry.get<components::Textureable>(
        static_cast<const entt::entity>(menu.entities[changedIndex]));
      currentSelectionTexture.hash = currentSelectionButton.normalPath;
      nextSelectionTexture.hash = nextSelectionButton.selectedPath;
      menu.selectedIndex = changedIndex;
    });
  }

  void InitMenu(engine::Registry &registry, engine::Registry::entity_type entity) {
    _registry.view<Menu>().each([this](auto entity, auto& menu){
      if (menu.selectedIndex >= 0 && menu.selectedIndex < menu.entities.size()) {
        auto& currentSelectionButton = _registry.get<Button>(
          static_cast<const entt::entity>(menu.entities[menu.selectedIndex]));
        auto& currentSelectionTexture = _registry.get<components::Textureable>(
          static_cast<const entt::entity>(menu.entities[menu.selectedIndex]));
        currentSelectionTexture.hash = currentSelectionButton.selectedPath;
      }
      LOG("MENU CONSTRUCTED");
    });
  }

  void SelectMenuEntry() {
    if (Input::Instance().IsKeyPressed("select")) {
      _registry.view<Menu>().each([this](auto entity, auto& menu) {
        auto& action = _registry.get<Action>(static_cast<const entt::entity>(menu.entities[menu.selectedIndex]));
        action.function();
      });
    }
  }

public:
  explicit SceneStart(engine::Game& game) : Scene(game, SCENE_NAME) {};

  void Init() override {
    _registry.on_construct<Menu>().connect<&SceneStart::InitMenu>(this);

    auto startButton = _registry.create();
    _registry.emplace<components::Textureable>(startButton, &startButtonPath);
    _registry.emplace<Button>(startButton, &startButtonPath,  &startButtonSelectedPath);
    _registry.emplace<components::Size>(startButton, 341.0f, 345.0f);
    _registry.emplace<components::Position>(startButton, 436.0f, 89.0f);
    _registry.emplace<Action>(startButton, "change_scene", [this]{
      LOG("START");
      _game.SetCurrentScene("game");
    });

    auto exitButton = _registry.create();
    _registry.emplace<components::Textureable>(exitButton, &exitButtonPath);
    _registry.emplace<Button>(exitButton, &exitButtonPath,  &exitButtonSelectedPath);
    _registry.emplace<components::Size>(exitButton, 341.0f, 345.0f);
    _registry.emplace<components::Position>(exitButton, 530.0f, 89.0f);
    _registry.emplace<Action>(exitButton, "exit_game", []{ LOG("EXIT"); });


    auto background = _registry.create();
    _registry.emplace<components::Textureable>(background, &backgroundPath);
    _registry.emplace<components::Size>(background, engine::Config::Instance().GetSetting<unsigned int>("windowSizeX"),
                                        engine::Config::Instance().GetSetting<unsigned int>("windowSizeY"));
    _registry.emplace<components::Position>(background, 0.0f, 0.0f);

    auto startMenu = _registry.create();
    _registry.emplace<Menu>(startMenu, 0, std::vector<unsigned int>{static_cast<unsigned int>(startButton),
                                                                    static_cast<unsigned int>(exitButton)});

    AddSystem(std::make_unique<systems::TextureRenderer>(_game, _registry));
  }

  void Update(float s) override {
    engine::Scene::Update(s);
    ButtonSystem();
    SelectMenuEntry();
  };
};
}

#endif //ECSGAMELOOP_SCENESTART_HPP
