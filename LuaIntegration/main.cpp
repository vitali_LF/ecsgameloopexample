#include <engine/Engine.hpp>
#include <engine/FrameworkIncludes.hpp>
#include "scenes/SceneStart.hpp"



void ProcessEvents(engine::Game& game) {
  Event event{};
  auto& window = game.GetWindow();
  if (Input::Instance().IsKeyPressed("quit")) window.close();
  while (window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) window.close();
  }
}

int main(int argc, const char **argv) {
  /*** Create Engine. It manages the main loop ***/
  engine::Engine engine;

  engine::GamePtr game = std::make_shared<engine::Game>();
  engine._onStepSubscription.connect([game](float) { ProcessEvents(*game); });
  engine.SetGame(game);
  engine.AddModules(std::make_shared<Module>());

  game->GetConfig().values["game"]["ball"]["spawn"]["position"]["player2"] = {{"x", 10}, {"y", 20}};
  game->GetConfig().values["game"]["ball"]["spawn"]["position"]["player1"] = {{"x", 10}, {"y", 20}};
  auto mainMenuScene = std::make_shared<scenes::SceneStart>(*game);
  game->AddScene(mainMenuScene);

  game->SetCurrentScene(mainMenuScene->GetName());
  engine.Run();
  return 0;
}
