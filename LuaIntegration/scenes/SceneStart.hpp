#ifndef ECSGAMELOOP_SCENESTART_HPP
#define ECSGAMELOOP_SCENESTART_HPP


#include <engine/Scene.hpp>
#include <engine/core_system/Resources.hpp>
#include <engine/core_system/Input.hpp>
#include <engine/ecs/components/Components.hpp>
#include <engine/ecs/systems/Systems.hpp>
#include <utility>

using namespace engine;

constexpr HashedString backgroundPath {"resources/Background.png"};

static const char* const SCENE_NAME = "main_menu";

namespace scenes {
class SceneStart : public engine::Scene {

public:
  explicit SceneStart(engine::Game& game) : Scene(game, SCENE_NAME) {};

  void Init() override {

    auto background = _registry.create();
    _registry.emplace<ecs::components::Textureable>(background, &backgroundPath);
    _registry.emplace<ecs::components::Size>(background, engine::Config::Instance().windowSizeX, engine::Config::Instance().windowSizeY);
    _registry.emplace<ecs::components::Position>(background, 0, 0);

    AddSystem(std::make_unique<ecs::systems::TextureRenderer>(_game, _registry));
  }

  void Update(float s) override {
    engine::Scene::Update(s);
  };
};
}

#endif //ECSGAMELOOP_SCENESTART_HPP
