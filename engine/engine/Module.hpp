#ifndef ECSGAMELOOP_MODULE_HPP
#define ECSGAMELOOP_MODULE_HPP

#include "Game.hpp"
#include "tools/HelperFunctions.hpp"

namespace engine {

class Module : public Game {

public:
  Module();
  Module(const Vector2& size, std::string  title);

  const Vector2& GetSize() const;
  const std::string& GetTitle() const;

private:
  Vector2 _size{640, 640};
  std::string _title{"TestModule"};
};

}


#endif //ECSGAMELOOP_MODULE_HPP
