#pragma once

namespace engine::ecs::components {

struct Collider2D {
  float width;
  float height;
};

}
