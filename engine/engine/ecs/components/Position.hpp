#ifndef ENGINE_ECS_COMPONENTS_POSITION_H
#define ENGINE_ECS_COMPONENTS_POSITION_H


namespace engine::ecs::components {
struct Position {
  float x;
  float y;
};
struct Offset {
  float x;
  float y;
};
}


#endif //ENGINE_ECS_COMPONENTS_POSITION_H
