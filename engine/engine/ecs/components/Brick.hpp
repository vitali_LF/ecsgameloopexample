#pragma once
#include <SFML/Graphics/Color.hpp>

using Uint8 = unsigned char;

namespace engine::ecs::components {
struct Color {
  unsigned char r, g, b, a;
  operator sf::Color() {
    return {r,g,b,a};
  }

  Color& operator=(const sf::Color& rhs) {
    r = rhs.r;
    g = rhs.g;
    b = rhs.b;
    a = rhs.a;
    return *this;
  }

  Color& Lerp(Color rhs) {
    r = (rhs.r + r) / 2;
    g = (rhs.g + g) / 2;
    b = (rhs.b + b) / 2;
    a = (rhs.a + a) / 2;
    return *this;
  }

  Color(const sf::Color& rhs) {
    r = rhs.r;
    g = rhs.g;
    b = rhs.b;
    a = rhs.a;
  }

  explicit Color(Uint8 r = 255, Uint8 g = 255, Uint8 b = 255, Uint8 a = 255) : r(r), g(g), b(b), a(a) {}
};

struct Brick {
};
}

