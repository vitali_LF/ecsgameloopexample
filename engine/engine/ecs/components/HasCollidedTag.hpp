#ifndef ENGINE_ECS_COMPONENTS_HASCOLLIDEDTAG_HPP
#define ENGINE_ECS_COMPONENTS_HASCOLLIDEDTAG_HPP

#include <engine/FrameworkNaming.hpp>

namespace engine::ecs {
namespace components {
struct HasCollidedTag {
  engine::Entity thisEntity{0};
  engine::Entity otherEntity{0};
};
struct LastCollisionByPlayer {
  engine::Entity playerEntity{0};
};
}
}


#endif //ENGINE_ECS_COMPONENTS_HASCOLLIDEDTAG_HPP
