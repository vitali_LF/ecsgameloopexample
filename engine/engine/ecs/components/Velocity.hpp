#pragma once

namespace engine {
namespace ecs {
namespace components {

struct Velocity {
  float x;
  float y;
};

}
}
}