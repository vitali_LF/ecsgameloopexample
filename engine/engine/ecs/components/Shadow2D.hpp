#ifndef ENGINE_ECS_COMPONENTS_SHADOW2D_H
#define ENGINE_ECS_COMPONENTS_SHADOW2D_H

namespace engine::ecs::components {
    struct Shadow2D {
      float x;
      float y;
    };
}


#endif //ENGINE_ECS_COMPONENTS_SHADOW2D_H
