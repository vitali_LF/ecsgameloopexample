#pragma once

namespace engine {
namespace ecs {
namespace components {

struct Drag {
  float percentOfVelocityThatWillStay = 1;
};

}
}
} 