#ifndef ENGINE_ECS_COMPONENTS_PADDLE_HPP
#define ENGINE_ECS_COMPONENTS_PADDLE_HPP

namespace engine::ecs {
  namespace components {
    struct Paddle {};
  }
}

#endif //ENGINE_ECS_COMPONENTS_PADDLE_HPP
