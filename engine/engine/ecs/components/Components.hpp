#ifndef ENGINE_ECS_COMPONENTS_HPP
#define ENGINE_ECS_COMPONENTS_HPP

#include "Brick.hpp"
#include "Collider2D.hpp"
#include "Drag.hpp"
#include "HasCollidedTag.hpp"
#include "Paddle.hpp"
#include "Position.hpp"
#include "Shadow2D.hpp"
#include "Size.hpp"
#include "Textureable.hpp"
#include "Velocity.hpp"

namespace engine::ecs::components {
struct Player {
  unsigned index;
};

struct Player1 {};
struct Player2 {};
struct AI {};
struct BoxIso3D {};
struct Destructible {};
struct Bounceable {};
struct Component {
  std::string name;
};
struct Damage {
  engine::Entity player{ 0 };
  float alpha{ 0.5f };
};
struct Score {
  int player1 = 0;
  int player2 = 0;
};

struct Index {
  size_t x;
  size_t y;

  static Index Zero() {
    return Index{size_t(0), size_t(0)};
  }

  bool operator==(const Index& rhs) const {
    return x == rhs.x &&
           y == rhs.y;
  }

  bool operator!=(const Index& rhs) const {
    return !(rhs == *this);
  }
};

struct Finite {
  float _duration{0.f};
  bool _active{true};
  bool _started{false};
  bool _finished{false};
};

struct Sequence {
  std::vector<engine::Entity> queue;
};

struct EntityTarget {
  engine::Entity entityId{0};
};

struct TextureComponent {
  std::string texturePath;
};

/// Action
struct ShakeCamera {
  enum Direction {
    Left, Right
  };
  float duration;
  Direction direction;
};
}

#endif // ENGINE_ECS_COMPONENTS_HPP
