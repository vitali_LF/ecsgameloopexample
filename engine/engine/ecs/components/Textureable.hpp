#ifndef ENGINE_ECS_COMPONENTS_TEXTUREABLE_H
#define ENGINE_ECS_COMPONENTS_TEXTUREABLE_H

#include <engine/FrameworkNaming.hpp>

namespace engine::ecs {
namespace components {

struct Textureable {
  const engine::HashedString* hash;
};

}
}


#endif //ENGINE_ECS_COMPONENTS_TEXTUREABLE_H
