#ifndef ENGINE_ECS_COMPONENTS_SIZE_HPP
#define ENGINE_ECS_COMPONENTS_SIZE_HPP

namespace engine::ecs::components {
struct Size {
  float width; float height;
  Size operator/(const float& scalar) {
    if (scalar == 0) return *this;
    return {this->width / scalar, this->height / scalar};
  }
  Size() : width(0), height(0) {}

  Size(float width, float height) : width(width), height(height) {}
};

struct SizeInPercent {
  float width;
  float height;
};

}

#endif //ENGINE_ECS_COMPONENTS_SIZE_HPP
