#include <SFML/Graphics/RectangleShape.hpp>
#include "RenderShadows2D.hpp"
#include "../components/Components.hpp"

using namespace engine::ecs;

namespace engine {
namespace ecs {
namespace systems {

void RenderShadows2D::Update(float dt) {
  sf::RectangleShape shadow;
  shadow.setFillColor(sf::Color(0, 0, 0, 30));
  auto view = _registry.view<components::Position, components::Size, components::Shadow2D>();
  view.each([&](auto entity, auto& pos, auto& size, auto& shadow2d) {
    shadow.setSize({(float) size.width, (float) size.height});
    shadow.setPosition({(float) pos.x - 8, (float) pos.y + 8});
    _game.GetWindow().draw(shadow);
  });
}

}
}
}
