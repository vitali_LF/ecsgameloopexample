#include "../components/Components.hpp"
#include "SpawnOverTime.hpp"

namespace engine::ecs::systems {

SpawnOverTime::SpawnOverTime(engine::Game& game, engine::Registry& registry,
                             std::function<engine::Entity(Registry&)> prototype, float spawnEverySec)
  : System(game, registry), _prototype(std::move(prototype)), _spawnEverySecTime(spawnEverySec) {}

void SpawnOverTime::Update(float dt) {
  UpdateTime(dt);
  if (IsTimeToSpawn() && CheckConditions()) {
    Spawn();
    ResetTime();
  }
}

void SpawnOverTime::ResetTime() {
  _timeSinceLastSpawn = 0;
}

void SpawnOverTime::UpdateTime(float dt) {
  _timeSinceLastSpawn += dt;
}

bool SpawnOverTime::IsTimeToSpawn() const {
  return _timeSinceLastSpawn >= _spawnEverySecTime;
}

bool SpawnOverTime::CheckConditions() {
  return _condition.accumulate(false, std::logical_or<bool>{}).operator()();
}

void SpawnOverTime::Spawn() {
  LOG("Spawn");
  auto entity = _prototype(_registry);
  _afterSpawn(_registry, entity);
}

void SpawnOverTime::AddCondition(const std::function<bool(void)>& condition) {
  _condition.connect(condition);
}

void SpawnOverTime::SubscribeBeforeSpawn(const std::function<void(engine::Registry&, engine::Entity)>& action) {
  _afterSpawn.connect(action);
}

}