#include "ParseSprite.hpp"
#include <engine/ecs/components/Components.hpp>
#include <engine/core_system/Resources.hpp>

namespace engine::ecs::systems {

ParseSprite::ParseSprite(engine::Game& game, engine::Registry& registry, std::string spritePath)
  : System(game, registry), _spritePath(std::move(spritePath)) {}

void ParseSprite::Update(float dt) {
  auto brickSize = static_cast<float>(engine::Config::Instance().brickSize);
  auto brickPadding = static_cast<float>(engine::Config::Instance().brickPadding);
  auto startX = brickSize * 8 + brickPadding * 8;
  auto startY = brickSize * 1 + brickPadding * 1;
  auto texture = &Resources::Instance().GetTexture(_spritePath).handle();
  auto image = texture->get()->copyToImage();
  for (unsigned int y = 0; y < image.getSize().y; y++) {
    for (unsigned int x = 0; x < image.getSize().x; x++) {
      auto pixel = image.getPixel(x, y);
      if (pixel.a < 0.01) continue;

      float positionX = startX + brickSize * x + x * brickPadding;
      float positionY = startY + brickSize * y + y * brickPadding;

      auto entity = _registry.create();
      _registry.emplace<components::Component>(entity, "Brick");
      _registry.emplace<components::Brick>(entity);
      _registry.emplace<components::Color>(entity, pixel.r, pixel.g, pixel.b, pixel.a);
      _registry.emplace<components::Shadow2D>(entity);
      _registry.emplace<components::Destructible>(entity);
      _registry.emplace<components::Position>(entity, positionX, positionY);
      _registry.emplace<components::Collider2D>(entity, brickSize, brickSize);
      _registry.emplace<components::Size>(entity, brickSize, brickSize);
    }
  }
  SetFinished(true);
}


}