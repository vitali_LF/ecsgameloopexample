#ifndef ENGINE_ECS_COMPONENTS_RENDERSHADOWS2D_HPP
#define ENGINE_ECS_COMPONENTS_RENDERSHADOWS2D_HPP

#include <engine/System.hpp>

namespace engine{
namespace ecs{
namespace systems {

class RenderShadows2D : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override;
};

}
}
}

#endif //ENGINE_ECS_COMPONENTS_RENDERSHADOWS2D_HPP
