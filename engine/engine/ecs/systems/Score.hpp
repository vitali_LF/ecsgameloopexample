#ifndef ENGINE_ECS_SYSTEMS_SCORE_HPP
#define ENGINE_ECS_SYSTEMS_SCORE_HPP

#include <engine/System.hpp>

namespace engine::ecs::systems {

class Score : public engine::System {
public:
  using engine::System::System;

  Score(engine::Game &game, engine::Registry &registry);

  void Update(float dt) override;
  engine::Font font;
  engine::Text player1Text;
  engine::Text player2Text;
};
}


#endif //ENGINE_ECS_SYSTEMS_SCORESYSTEM_HPP
