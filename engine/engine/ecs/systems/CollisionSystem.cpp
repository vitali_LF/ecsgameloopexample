#include "CollisionSystem.hpp"

namespace engine::ecs::systems {

CollisionSystem::CollisionSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {
  _soundCollisionClick = std::make_shared<engine::Sound>("sounds/klick.wav");
  _soundCollisionPlop = std::make_shared<engine::Sound>("sounds/plop.flac");
}

void CollisionSystem::Update(float dt) {
  auto view = _registry.view<ecs::components::Position, ecs::components::Collider2D, ecs::components::Velocity>();
  for (auto entityFirst : view) {
    auto& position = view.get<ecs::components::Position>(entityFirst);
    auto& coll = view.get<ecs::components::Collider2D>(entityFirst);
    auto&& velocity = view.get<ecs::components::Velocity>(entityFirst);

    CheckBorders(entityFirst);

    auto viewColliders = _registry.view<ecs::components::Position, ecs::components::Collider2D, ecs::components::Component>();
    for (auto entitySecond : viewColliders) {
      if (entitySecond == entityFirst) continue;

      auto& position2 = viewColliders.get<ecs::components::Position>(entitySecond);
      auto& coll2 = viewColliders.get<ecs::components::Collider2D>(entitySecond);
      auto& comp2 = viewColliders.get<ecs::components::Component>(entitySecond);
      RectWrapper left({position.x, position.y}, {coll.width, coll.height});
      RectWrapper right({position2.x, position2.y}, {coll2.width, coll2.height});

      if (engine::tools::isIntersecting(left, right)) {
        _registry.emplace_or_replace<ecs::components::HasCollidedTag>(entitySecond, entitySecond, entityFirst);
        if (_registry.all_of<ecs::components::Bounceable>(entityFirst)) engine::tools::Bounce(left, right, velocity);
        if (_registry.all_of<ecs::components::Paddle>(entitySecond)) _soundCollisionClick->Play();
        if (_registry.all_of<ecs::components::Brick>(entitySecond)) _soundCollisionPlop->Play();
        if (_registry.all_of<ecs::components::Player>(entityFirst)) {
          _registry.emplace_or_replace<ecs::components::LastCollisionByPlayer>(entitySecond, entityFirst);
        }
      }
    }
  }
}

void CollisionSystem::CheckBorders(engine::Entity enttId) {

  auto& position = _registry.get<ecs::components::Position>(enttId);
  auto& coll = _registry.get<ecs::components::Collider2D>(enttId);
  auto& velocity = _registry.get<ecs::components::Velocity>(enttId);

  auto width = engine::Config::Instance().GetSetting<unsigned int>("windowSizeX");
  auto height = engine::Config::Instance().GetSetting<unsigned int>("windowSizeY");

  if (position.y + coll.height >= height) {
    velocity.y = -std::abs(velocity.y);
  } else if (position.y <= 0) {
    velocity.y = std::abs(velocity.y);
  }
  if (position.x + coll.width >= width) {
    velocity.x = -std::abs(velocity.x);
  } else if (position.x <= 0) {
    velocity.x = std::abs(velocity.x);
  }
}

}