//
// Created by Artjom Djaditschkin on 23.06.18.
//

#ifndef ENGINE_ECS_SYSTEMS_DESTROYONCOLLISIONSYSTEM_H
#define ENGINE_ECS_SYSTEMS_DESTROYONCOLLISIONSYSTEM_H

#include <engine/System.hpp>
#include <engine/core_system/Config.hpp>
#include <engine/tools/HelperFunctions.hpp>
#include "../components/Components.hpp"

namespace engine::ecs::systems {

class DestroyOnCollisionSystem : public engine::System {

  using engine::System::System;
  void Update(float dt) override;

};
}

#endif //ENGINE_ECS_SYSTEMS_DESTROYONCOLLISIONSYSTEM_H
