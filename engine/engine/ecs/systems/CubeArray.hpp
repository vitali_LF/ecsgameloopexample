#pragma once


#include <engine/System.hpp>

namespace engine::ecs::systems {

class CubeArray : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override;
};

} // systems
