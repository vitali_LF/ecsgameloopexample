#ifndef ENGINE_ECS_SYSTEMS_TEXTURERENDER_HPP
#define ENGINE_ECS_SYSTEMS_TEXTURERENDER_HPP

#include <engine/System.hpp>

namespace engine {
namespace ecs {
namespace systems {

class TextureRenderer : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override;
};

}
}
}

#endif //ENGINE_ECS_SYSTEMS_TEXTURERENDER_HPP
