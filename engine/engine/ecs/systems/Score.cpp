#include "Score.hpp"
#include <engine/FrameworkIncludes.hpp>

namespace engine::ecs::systems {

ecs::systems::Score::Score(engine::Game& game, engine::Registry& registry) : System(game, registry) {
  font.loadFromFile(Resources::Instance().GetPath("fonts/open-sans/OpenSans-Regular.ttf"));

  player1Text.setFont(font);
  player1Text.setCharacterSize(32);
  player1Text.setFillColor(engine::Color::White);
  player1Text.setPosition(2, engine::Config::Instance().GetSetting<float>("displayHeight1Percent") * 90);

  player2Text.setFont(font);
  player2Text.setCharacterSize(32);
  player2Text.setFillColor(engine::Color::White);
  player2Text.setPosition(engine::Config::Instance().GetSetting<float>("displayWidth1Percent") * 80,
                          engine::Config::Instance().GetSetting<float>("displayHeight1Percent") * 90);
}

void Score::Update(float dt) {
  auto scoreView = _game.GetRegistry().view<ecs::components::Score>();
  scoreView.each([&](auto entity, auto&& gameScore) {
    player1Text.setString(std::string("Player1: ") + std::to_string(gameScore.player1));
    player2Text.setString(std::string("Player2: ") + std::to_string(gameScore.player2));
    _game.GetWindow().draw(player1Text);
    _game.GetWindow().draw(player2Text);
  });
}

}