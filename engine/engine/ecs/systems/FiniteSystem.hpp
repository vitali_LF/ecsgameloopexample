#pragma once

#include <engine/System.hpp>
#include <engine/ecs/components/Components.hpp>

namespace engine {
namespace ecs {
namespace systems {

class FiniteSystem : public engine::System {
  using engine::System::System;
public:
  void Update(float dt) override {
    _registry.view<components::Finite>().each([&](auto entity, auto&& finite) {
      auto& comp = _registry.get<components::Component>(entity);
      LOG("FiniteSystem Update: " << comp.name << " Active: " << finite._active );
      if (finite._active) finite._duration -= dt;
      if (finite._duration <= 0.0f) {
        finite._finished = true;
      }
    });
  }
};

class FiniteCleanerSystem : public engine::System {
  using engine::System::System;
public:
  void Update(float dt) override {
    _registry.view<components::Finite>().each([&](auto entity, auto&& finite) {
      if (finite._finished) {
        _registry.destroy(entity);
      }
    });
  }
};

}
}
} 