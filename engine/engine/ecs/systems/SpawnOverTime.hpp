
#ifndef ENGINE_ECS_SYSTEMS_SPAWNOVERTIME_H
#define ENGINE_ECS_SYSTEMS_SPAWNOVERTIME_H

#include <engine/System.hpp>
#include <engine/tools/HelperFunctions.hpp>
#include <cmath>
#include <utility>

namespace engine::ecs::systems {

class SpawnOverTime : public engine::System {
public:
  SpawnOverTime(engine::Game& game, engine::Registry& registry,
                std::function<engine::Entity(Registry&)> prototype,
                float spawnEverySec =  0.1f);

  void Update(float dt) override;

  void ResetTime();

  void UpdateTime(float dt);

  bool IsTimeToSpawn() const;

  bool CheckConditions();

  void Spawn();

  void AddCondition(const std::function<bool(void)>& condition);

  void SubscribeBeforeSpawn(const std::function<void(engine::Registry&, engine::Entity)>& action);

protected:
  engine::Signal<bool(void)> _condition;
  engine::Signal<void(engine::Registry&, engine::Entity)> _afterSpawn;
  std::function<engine::Entity(Registry&)> _prototype;
  float _timeSinceLastSpawn{0};
  float _spawnEverySecTime{0};
};

}

#endif //ENGINE_ECS_SYSTEMS_SPAWNOVERTIME_H