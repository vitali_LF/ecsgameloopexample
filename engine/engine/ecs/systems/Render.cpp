#include <SFML/Graphics/RectangleShape.hpp>
#include "Render.hpp"
#include "../components/Components.hpp"
#include <engine/core_system/Resources.hpp>


namespace engine::ecs::systems {

void Render::Update(float dt) {
  {
    sf::RectangleShape r;
    _registry
      .view<components::Position, components::Size, components::Color>(entt::exclude<components::TextureComponent>)
      .each([&](auto entity, auto& pos, auto& size, auto& color) {
        // _registry.get_or_emplace<components::SizeInPercent>(entity, 0.0f, 0.0f);
        r.setSize({(float) size.width, (float) size.height});
        r.setPosition({(float) pos.x, (float) pos.y});
        r.setFillColor(color);
        _game.GetWindow().draw(r);
      });
  }
  {
    sf::RectangleShape r;
    _registry.view<components::Position, components::Size, components::Color, components::TextureComponent>()
             .each([&](auto entity, auto& pos, auto& size, auto& color, auto& textureComponent) {
               r.setSize({(float) size.width, (float) size.height});
               r.setPosition({(float) pos.x, (float) pos.y});
               r.setTexture(engine::Resources::Instance().GetTexture(textureComponent.texturePath).handle().get());
               _game.GetWindow().draw(r);
             });
  }
}

}