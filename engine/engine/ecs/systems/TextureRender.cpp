#include "TextureRender.hpp"
#include "../components/Components.hpp"
#include <engine/core_system/Resources.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

namespace engine {
namespace ecs {
namespace systems {


void TextureRenderer::Update(float dt) {
  auto view = _registry.view<components::Textureable, components::Size, components::Position>();
  for (auto entity : view) {
    sf::RectangleShape rect;
    auto& position = view.get<components::Position>(entity);
    auto& size = view.get<components::Size>(entity);
    auto& texture = view.get<components::Textureable>(entity);
    const sf::Texture* pTexture = Resources::Instance().GetTexture(*texture.hash).handle().get();
    rect.setTexture(pTexture);
    rect.setSize({size.width, size.height});
    rect.setPosition(position.x, position.y);
    _game.GetWindow().draw(rect);
  }
}

}
}
}
