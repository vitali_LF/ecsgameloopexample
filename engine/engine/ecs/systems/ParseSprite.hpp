#ifndef ENGINE_ECS_SYSTEMS_PARSESPRITE_HPP
#define ENGINE_ECS_SYSTEMS_PARSESPRITE_HPP

#include <engine/System.hpp>

namespace engine::ecs::systems {

class ParseSprite : public engine::System {
public:
  ParseSprite(engine::Game& game, engine::Registry& registry, std::string spritePath);

  void Update(float dt) override;

private:
  std::string _spritePath;
};

}


#endif //ENGINE_ECS_SYSTEMS_PARSESPRITE_HPP
