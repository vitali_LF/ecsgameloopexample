#ifndef ENGINE_ECS_SYSTEMS_RENDER_HPP
#define ENGINE_ECS_SYSTEMS_RENDER_HPP

#include <engine/System.hpp>

namespace engine::ecs::systems {

class Render : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override;
};

}

#endif //ENGINE_ECS_SYSTEMS_RENDER_HPP
