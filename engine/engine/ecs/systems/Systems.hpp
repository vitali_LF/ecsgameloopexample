#ifndef ENGINE_ECS_SYSTEMS_SYSTEMS_HPP
#define ENGINE_ECS_SYSTEMS_SYSTEMS_HPP

#include "../components/Components.hpp"
#include "DestroyOnCollisionSystem.hpp"
#include "CollisionSystem.hpp"
#include "FiniteSystem.hpp"
#include "ParseSprite.hpp"
#include "Render.hpp"
#include "RenderShadows2D.hpp"
#include "Score.hpp"
#include "SpawnOverTime.hpp"
#include "TextureRender.hpp"

#include <engine/System.hpp>
#include <engine/Scene.hpp>
#include <engine/core_system/Config.hpp>
#include <engine/tools/HelperFunctions.hpp>
#include <engine/core_system/Resources.hpp>


namespace engine::ecs::systems {

class CameraRotationSystem : public engine::System {
public:
  CameraRotationSystem(engine::Game& game, engine::Registry& registry, engine::Scene& scene)
    : System(game, registry), _sceneCurrent(scene) {
    _registry.on_construct<components::ShakeCamera>()
             .connect<&CameraRotationSystem::StartAnimation>(this);
      _view = _game.GetWindow().getView();
  }

  void StartAnimation(engine::Registry &registry, engine::Entity entity) {
    auto& shakeCamRef = _registry.get<components::ShakeCamera>(entity);
    float length = shakeCamRef.duration;
    float progress = length;
    _sceneCurrent
      .AddSystemLambdaFinite([this, entity, progress, length](engine::Game&, float dt, bool& finished) mutable {

      if(progress < 0) {
        finished = true;
        _registry.destroy(entity);
        return;
      };

      if (progress > length / 3 * 2) {
        _view.rotate(360 + 0.08f);
      } else if (progress > length / 3) {
        _view.rotate(360 - 0.16f);
      } else {
        _view.rotate(360 + 0.08f);
      }
      _game.GetWindow().setView(_view);

      progress -= dt;
    });
  }

  void Update(float dt) override { }

private:
  engine::Entity _entt;
  engine::Scene& _sceneCurrent;
  sf::View _view ;
  float _rotationIntencity{2};
};

class AnimationStarterSystem : public engine::System {
public:
  AnimationStarterSystem(engine::Game& game, engine::Registry& registry, engine::Scene& scene)
    : System(game, registry), _sceneCurrent(scene) {
    _registry.on_construct<components::Damage>()
             .connect<&AnimationStarterSystem::StartAnimation>(this);

  }

  void StartAnimation(engine::Registry &registry, engine::Entity entity) {
    _sceneCurrent.AddSystemLambdaFinite([this, entity](engine::Game&, float dt, bool& finished) {
      auto& damageRef = _registry.get<components::Damage>(entity);
      auto displayWidth1P = engine::Config::Instance().GetSetting<float>("displayWidth1Percent");
      auto displayHeight1P = engine::Config::Instance().GetSetting<float>("displayHeight1Percent");
      if (damageRef.player == engine::Entity(1)) {
        _rect.setPosition(0, 0);
        _rect.setFillColor(sf::Color(230, 128, 114, (Uint8) (255 * damageRef.alpha)));
      } else if (damageRef.player == engine::Entity(2)) {
        _rect.setPosition(displayWidth1P * 94.5f, 0);
        _rect.setFillColor(sf::Color(20, 66, 144, (Uint8) (255 * damageRef.alpha)));
      }
      _rect.setSize({displayWidth1P * 5.5f, displayHeight1P * 100});
      _game.GetWindow().draw(_rect);

      damageRef.alpha -= dt;

      if (damageRef.alpha < 0) {
        _registry.destroy(entity);
        finished = true;
      }
    });
  }

  void Update(float dt) override { }

private:
  engine::Scene& _sceneCurrent;
  engine::RectangleShape _rect;
};

class Drag : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override {
    _registry.view<components::Velocity, components::Drag>().each([&](auto entity, auto &&vel, auto &&drag) {
      auto newVel = engine::Lerp(
        {vel.x, vel.y},
        {vel.x * drag.percentOfVelocityThatWillStay, vel.y * drag.percentOfVelocityThatWillStay},
        dt
      );
      vel.x = newVel.x;
      vel.y = newVel.y;
    });
  }
};

class ApplyVelocity : public engine::System {
public:
  using engine::System::System;

  void Update(float dt) override {
    _registry.view<components::Position, components::Velocity>().each([&](auto entity, auto&& pos, auto&& vel) {
      pos.x += vel.x * dt;
      pos.y += vel.y * dt;
    });
  };
};


class SequenceSystem : public engine::System {
public:
  SequenceSystem(engine::Game& game, engine::Registry& registry) : System(game, registry) {
    _registry.on_construct<components::Sequence>()
             .connect<&SequenceSystem::StartSequence>(this);
  }

  void StartSequence(engine::Registry& registry, engine::Entity entity) {
    auto&& sequence = registry.get<components::Sequence>(entity);
    auto&& component = registry.get<components::Component>(entity);
    auto&& finite = registry.get<components::Finite>(entity);
    if (!finite._started && finite._active) {
      LOG("Sequence " << component.name << " Starting it.");
      finite._started = true;
      auto&& finiteSeqElement = registry.get<components::Finite>(sequence.queue.front());
      finiteSeqElement._active = true;
    }
  }

public:
  void Update(float dt) override {
    _registry.view<components::Sequence, components::Finite, components::Component>()
             .each([&](auto entity, auto&& sequence, auto&& finiteThis, auto&& component) {
               LOG("Sequence Update");
               if (!finiteThis._started && finiteThis._active) {
                 LOG("Sequence " << component.name << " Starting it.");
                 finiteThis._started = true;
                 auto&& finiteSeqElement = _registry.get<components::Finite>(sequence.queue.front());
                 finiteSeqElement._active = true;
               }
               if (!finiteThis._active) return;
               // Sequence is empty ? delete it
               if (sequence.queue.empty()) {
                 finiteThis._duration = 0.0f;
                 return;
               } else {
                 finiteThis._duration = 1000000000.0f; // FIXME compute the sum of all the times
               }
               // Has first element finished ? delete it, and set the next element active
               auto&& finiteSeqElement = _registry.get<components::Finite>(sequence.queue.front());
               if (finiteSeqElement._finished) {
                 // LOG("PowerUp " << std::to_string(sequence.queue.front()) << " finished. Deleting it.");
                 sequence.queue.erase(sequence.queue.begin());
                 if (sequence.queue.empty()) return;
                 auto&& nextElement = _registry.get<components::Finite>(sequence.queue.front());
                 // LOG("Activating new element " << sequence.queue.front() << " " << component.name);
                 nextElement._active = true;
                 return;
               }
             });
  }
};

}
#endif //ENGINE_ECS_SYSTEMS_SYSTEMS_HPP
