#include "CubeArray.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/VertexBuffer.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include "../components/Components.hpp"


namespace engine::ecs::systems {


static sf::Vector2f CartesianToIsometric(float x, float y, float size) {
  return {
      (x - y) * size * 0.5f,
      (x + y) * size * 0.25f
    };
}

static sf::Vector2f IsometricToCartesian(float x, float y, float size) {
  return {
      ( x + (2.0f * y)) / size,
      (-x + (2.0f * y)) / size
    };
}


void CubeArray::Update(float dt) {
  std::vector<sf::Vertex> vertices;
  sf::VertexBuffer vertexBuffer;
  _registry.view<components::Position, components::Size, components::Offset, components::Color, components::Index, components::BoxIso3D>()
           .each([&](auto entity, auto& pos, auto& size, auto& offset, auto& color, auto& index) {

             // auto pos = pos1;
             // auto pos = CartesianToIsometric((float)index.x*4, (float)index.y*4, size1.width);

             // pos += sf::Vector2f{100.0f, 100.0f};
             // ERROR("Cube drawn");
             sf::Vertex v[18] = {
               // Left side cube
               {sf::Vector2f(-size.width, -offset.y) + sf::Vector2f(pos.x, pos.y),              color.Lerp(Color(200, 50, 50))},
               {sf::Vector2f(0.0f, 0.0f) + sf::Vector2f(pos.x, pos.y),                          color.Lerp(Color(200, 50, 50))},
               {sf::Vector2f(0.0f, size.height) + sf::Vector2f(pos.x, pos.y),                   color.Lerp(Color(200, 50, 50))},

               {sf::Vector2f(-size.width, -offset.y) + sf::Vector2f(pos.x, pos.y),              color.Lerp(Color(20, 50, 250))},
               {sf::Vector2f(-size.width, size.height - offset.y) + sf::Vector2f(pos.x, pos.y), color.Lerp(Color(20, 50, 250))},
               {sf::Vector2f(0.0f, size.height) + sf::Vector2f(pos.x, pos.y),                   color.Lerp(Color(20, 50, 250))},

               // Right side cube
               {sf::Vector2f(0.0f, -offset.y) + sf::Vector2f(pos.x, pos.y),                     color.Lerp(Color(20, 250, 50))},
               {sf::Vector2f(size.width, 0.0f) + sf::Vector2f(pos.x, pos.y),                    color.Lerp(Color(20, 250, 50))},
               {sf::Vector2f(size.width, size.height) + sf::Vector2f(pos.x, pos.y),             color.Lerp(Color(20, 250, 50))},

               {sf::Vector2f(0.0f, -offset.y) + sf::Vector2f(pos.x, pos.y),                     color.Lerp(Color(20, 21, 50))},
               {sf::Vector2f(0.0f, size.height - offset.y) + sf::Vector2f(pos.x, pos.y),        color.Lerp(Color(20, 21, 50))},
               {sf::Vector2f(size.width, size.height) + sf::Vector2f(pos.x, pos.y),             color.Lerp(Color(20, 21, 50))}
             };
             for (size_t i = 0; i < 6; i++) {
               vertices.push_back(v[i]);
             }
           });

  vertexBuffer.setUsage(sf::VertexBuffer::Usage::Stream);
  vertexBuffer.setPrimitiveType(sf::Triangles);

  vertexBuffer.create(vertices.size());
  vertexBuffer.update(vertices.data());

  sf::RenderTarget& target = _game.GetWindow();
  sf::RenderStates states = sf::RenderStates::Default;
  target.draw(vertexBuffer, states);

}
}