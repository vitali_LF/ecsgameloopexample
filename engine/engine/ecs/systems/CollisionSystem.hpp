#ifndef ENGINE_ECS_SYSTEMS_COLLISIONSYSTEM
#define ENGINE_ECS_SYSTEMS_COLLISIONSYSTEM

#include <engine/System.hpp>
#include <engine/core_system/Sound.hpp>
#include <engine/ecs/components/Components.hpp>

#include <memory>

namespace engine::ecs::systems {

class CollisionSystem : public engine::System {
public:
  CollisionSystem(engine::Game& game, engine::Registry& registry);
  void Update(float dt) override;
  void CheckBorders(engine::Entity enttId);

private:
  std::shared_ptr<engine::Sound> _soundCollisionClick;
  std::shared_ptr<engine::Sound> _soundCollisionPlop;
};

}

#endif //ENGINE_ECS_SYSTEMS_COLLISIONSYSTEM
