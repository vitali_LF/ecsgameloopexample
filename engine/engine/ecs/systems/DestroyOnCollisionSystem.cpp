#include "DestroyOnCollisionSystem.hpp"

namespace engine::ecs::systems {

void DestroyOnCollisionSystem::Update(float dt) {
  auto collided = _registry.view<components::HasCollidedTag, components::Destructible>();
  for (auto&& item : collided) {
    _registry.destroy(item);
  }
}
}