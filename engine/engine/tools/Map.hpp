#pragma once

#include <map>

namespace engine::tools {

template <typename T1, typename T2>
bool HasEntry(const std::map<T1,T2>& map, T1 key) {
  auto it = map.find(key);
  return it != map.end();
};

}