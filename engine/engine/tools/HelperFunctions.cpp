#include "HelperFunctions.hpp"

namespace engine {
Vector2 Vector2::operator-(const Vector2& other) {
  return Vector2{this->x - other.x, this->y - other.y};
}

Vector2 Vector2::operator*(const float scale) {
  return Vector2{this->x * scale, this->y * scale};
}

Vector2 Vector2::operator+(const Vector2& other) {
  return Vector2{this->x + other.x, this->y + other.y};
}

Vector2 Lerp(Vector2 start, Vector2 end, float percent) {
  return (start + (end - start) * percent);
}

Vector2 CalculateDirectionVector(float hitPos) {
  if (hitPos == 0.5f) return up;
  int leftOrRight = 0;
  if (hitPos < 0.5f) leftOrRight = 1;
  if (hitPos > 0.5f) {
    leftOrRight = -1;
    hitPos -= 0.5f;
    hitPos = 0.5f - hitPos;
  }

  hitPos *= 2;

  Vector2 min{-0.75f, 0.25f};
  Vector2 max{0.0f, 1.0f};
  Vector2 result = Lerp(min, max, hitPos);
  result.x = result.x * leftOrRight;
  result.y = result.y * -1;
  return result;
}
}
namespace engine {
namespace tools {
/*
float CalculateCollisionPoint(Position posPaddle, Box2DCollider coll, Position posBall, Box2DCollider collBall) {
  auto minX = posPaddle.x;
  auto maxX = posPaddle.x + coll.size.width;
  auto sizeX = maxX - minX;
  auto ballPosX = posBall.x + collBall.size.width/2 - minX;
  return ballPosX / sizeX;
  //     5_______________25
  // ballPos    15
  //     sizeX=20
  //0_______________20
  //ballX  10
  // return 10 / 20 = 0.5
}
*/
}
}