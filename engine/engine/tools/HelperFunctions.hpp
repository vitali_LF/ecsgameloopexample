#pragma once

#include "../core_system/Config.hpp"
#include "../FrameworkNaming.hpp"

#include <cmath>


namespace engine {
struct Vector2 {
  float x;
  float y;

  Vector2 operator-(const Vector2& other);

  Vector2 operator+(const Vector2& other);

  Vector2 operator*(float scale);
};

static Vector2 up = Vector2{0.0f, 1.0f};

Vector2 Lerp(Vector2 start, Vector2 end, float percent);

Vector2 CalculateDirectionVector(float hitPos);

}
class RectWrapper {
  engine::Vector2 pos;
  engine::Vector2 size;
public:
  RectWrapper(const engine::Vector2& pos, const engine::Vector2& size) : pos(pos), size(size) {}

  [[nodiscard]] float Right() const { return pos.x + size.x; }

  [[nodiscard]] float Left() const { return pos.x; }

  [[nodiscard]] float Bottom() const { return pos.y + size.y; }

  [[nodiscard]] float Top() const {
    return pos.y; }

  [[nodiscard]] bool ContainsPosition(const engine::Vector2i& position) const {
    return position.y >= Top() && position.y <= Bottom() && position.x <= Right() && position.x >= Left();
  }
};

namespace engine::tools {

template<class T1, class T2>
bool isIntersecting(T1& mA, T2& mB) {
  return mA.Right() >= mB.Left() && mA.Left() <= mB.Right() &&
         mA.Bottom() >= mB.Top() && mA.Top() <= mB.Bottom();
}

template<class Brick, class Ball, class Velocity>
bool Bounce(Ball& mBall, Brick& mBrick, Velocity& ballVelocityObj) {
  float overlapLeft{mBall.Right() - mBrick.Left()};
  float overlapRight{mBrick.Right() - mBall.Left()};
  float overlapTop{mBall.Bottom() - mBrick.Top()};
  float overlapBottom{mBrick.Bottom() - mBall.Top()};

  bool ballFromLeft(std::abs(overlapLeft) < std::abs(overlapRight));
  bool ballFromTop(std::abs(overlapTop) < std::abs(overlapBottom));

  float minOverlapX{ballFromLeft ? overlapLeft : overlapRight};
  float minOverlapY{ballFromTop ? overlapTop : overlapBottom};

  auto ballVelocity = engine::Config::Instance().ballVelocityRelativeToRes;
  if (std::abs(minOverlapX) < std::abs(minOverlapY)) {
    ballVelocityObj.x = ballFromLeft ? -ballVelocity : ballVelocity;
  } else {
    ballVelocityObj.y = ballFromTop ? -ballVelocity : ballVelocity;
  }
  return true;
}

//float CalculateCollisionPoint(Position posPaddle, Box2DCollider coll, Position posBall, Box2DCollider collBall);

}