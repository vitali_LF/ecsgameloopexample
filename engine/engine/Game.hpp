#pragma once

#include <engine/tools/Map.hpp>
#include <engine/tools/HelperFunctions.hpp>
#include <engine/FrameworkNaming.hpp>

namespace engine {
class Scene;

class Game {
public:
  explicit Game(std::shared_ptr<sf::RenderWindow>  window);
  Game();

public:
  void SetWindow(const std::shared_ptr<sf::RenderWindow>& window);
  sf::RenderWindow& GetWindow() const;
  engine::Dispatcher& GetEventDispatcher();
  engine::Registry& GetRegistry();
  engine::Config& GetConfig();


  void Init();
  void StartGame();
  void Update(float s);
  bool HasScene(const std::string& label);
  void AddScene(const std::shared_ptr<Scene>& scene);
  void SetCurrentScene(const std::string& label);
  std::shared_ptr<Scene> GetCurrentScene();

  void DrawText(const std::string& text, Vector2 position, unsigned size = 24);
  void DrawRect(float x, float y, float width, float height, float alpha = 1.0f);

protected:
  void SetCurrentScene(const std::shared_ptr<Scene>& scene);

public:
  engine::Signal<void(void)> gameStarted;
  engine::Signal<void(void)> gameOver;

private:
  std::shared_ptr<Scene> _currentScene;
  std::map<std::string, std::shared_ptr<Scene>> _scenes;
  std::shared_ptr<engine::RenderWindow> _window;
  engine::Registry _registry;
  engine::Dispatcher _eventDispatcher;
  engine::Config* _config;


  Font _font;
  Text _text;
};

}