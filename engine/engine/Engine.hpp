#ifndef ECSGAMELOOP_ENGINE_MAINLOOP_HPP
#define ECSGAMELOOP_ENGINE_MAINLOOP_HPP

#include "Game.hpp"
#include "Module.hpp"

namespace engine {

class Engine {

public:

  void SetGame(const std::shared_ptr<Game>& game);
  void AddModules(const std::shared_ptr<Module>& module);

  sf::RenderWindow& GetWindow();

  int Run();
  nod::signal<void(float)> _onStepSubscription;

private:
  std::shared_ptr<sf::RenderWindow> _window;
  std::shared_ptr<Game> _game;
  std::vector<std::shared_ptr<Module>> _modules;
};
}

#endif //ECSGAMELOOP_ENGINE_MAINLOOP_HPP
