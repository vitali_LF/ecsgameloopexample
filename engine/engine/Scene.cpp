#include "Scene.hpp"

#include <utility>
#include "Game.hpp"

namespace engine {

Scene::Scene(Game& game, std::string  name) : _game(game), _name(std::move(name)) {}

void Scene::Init() {}

void Scene::Finish() {}

void Scene::Start() {
  _running = true;
}

void Scene::Pause() {
  _running = false;
}

std::string Scene::GetName() {
  return _name;
}

void Scene::Update(float dt) {
  if (!_running) return;

  for (auto&& system : _systems) {
    system->Update(dt);
  }

  for (auto&& system : _systemsEndless) {
    system(_game, dt);
  }

  //iterate call functions and delete if finished

  _systems.erase(
    std::remove_if(_systems.begin(), _systems.end(),
                   [](std::shared_ptr<engine::System>& system) -> bool {
      auto a = system->HasFinished();
                     return a;
                   }),
    _systems.end()
  );

  _systemsFinite.erase(
    std::remove_if(_systemsFinite.begin(), _systemsFinite.end(),
                   [this, dt](engine::SystemFinite& system) -> bool {
                     bool finished = false;
                     system(_game, dt, finished);
                     return finished;
                   }),
    _systemsFinite.end()
  );

}

void Scene::ResizeEvent(const engine::Vector2& size) {}

void Scene::AddSystem(const std::shared_ptr<System>& system) {
  _systems.push_back(system);
}

void Scene::AddSystemLambda(engine::SystemEndless system) {
  _systemsEndless.push_back(std::move(system));
}

void Scene::AddSystemLambdaFinite(engine::SystemFinite system) {
  _systemsFinite.push_back(std::move(system));
}

}

