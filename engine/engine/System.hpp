#ifndef ECSGAMELOOP_SYSTEM_HPP
#define ECSGAMELOOP_SYSTEM_HPP

#include "Game.hpp"

namespace engine {

class System {
public:
  System(Game& game, engine::Registry& registry);
  virtual void Update(float dt);

  bool HasFinished() const;

  void SetFinished(bool finished);

protected:
  bool _finished{false};
  Game& _game;
  engine::Registry& _registry;
  engine::Dispatcher& _eventDispatcher;
};

}


#endif //ECSGAMELOOP_SYSTEM_HPP
