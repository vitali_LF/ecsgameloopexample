#ifndef ENGINE_CORE_SYSTEMS_SOUND_HPP
#define ENGINE_CORE_SYSTEMS_SOUND_HPP

#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>

namespace engine {

class Sound {

public:
  Sound(const std::string& fileRelativePath);

  void Play();

  void Pause();

  void Stop();

protected:
  std::string _fileRelativePath;
  sf::SoundBuffer buffer;
  sf::Sound sound;
};

}
#endif //ENGINE_CORE_SYSTEMS_SOUND_HPP
