#include "Sound.hpp"
#include "Resources.hpp"

namespace engine {

Sound::Sound(const std::string& fileRelativePath) : _fileRelativePath(fileRelativePath) {
  auto path = Resources::Instance().GetPath(_fileRelativePath);
  buffer.loadFromFile(path);
  sound.setBuffer(buffer);
}

void Sound::Play() {
  sound.play();
}

void Sound::Pause() {
  sound.pause();
}

void Sound::Stop() {
  sound.stop();
}
}
