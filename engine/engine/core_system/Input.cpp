#include <engine/core_system/Input.hpp>

namespace engine {

Input::Input() {
  std::string player1("_p1");
  inputMapping["left" + player1] = Input::Key{engine::Keyboard::Left, false};
  inputMapping["right" + player1] = Input::Key{engine::Keyboard::Right, false};
  inputMapping["up" + player1] = Input::Key{engine::Keyboard::Up, false};
  inputMapping["down" + player1] = Input::Key{engine::Keyboard::Down, false};
  inputMapping["menu" + player1] = Input::Key{engine::Keyboard::Escape, false};
  inputMapping["select" + player1] = Input::Key{engine::Keyboard::Return, false};
  inputMapping["quit" + player1] = Input::Key{engine::Keyboard::Q, false};
  inputMapping["create_ball" + player1] = Input::Key{engine::Keyboard::B, false};
  inputMapping["power_up" + player1] = Input::Key{engine::Keyboard::P, false};
  inputMapping["grow" + player1] = Input::Key{engine::Keyboard::G, false};
  inputMapping["sequence" + player1] = Input::Key{engine::Keyboard::I, false};

  std::string player2("_p2");
  inputMapping["left" + player2] = Input::Key{engine::Keyboard::Left, false};
  inputMapping["right" + player2] = Input::Key{engine::Keyboard::Right, false};
  inputMapping["up" + player2] = Input::Key{engine::Keyboard::Up, false};
  inputMapping["down" + player2] = Input::Key{engine::Keyboard::Down, false};
}

void Input::Update() {
  for (auto&& input : inputMapping) {
    bool pressed = sf::Keyboard::isKeyPressed(input.second.key);
    if (input.second.pressed != pressed) {
      input.second.pressed = pressed;
      onNewInput(input.first, input.second.pressed);
    }
  }
}

bool Input::IsKeyPressed(std::string command, unsigned player) {
  command = command + "_p" + std::to_string(player);

  auto it = inputMapping.find(command);
  if (it == inputMapping.end()) return false;

  auto keyData = inputMapping.at(command);
  return sf::Keyboard::isKeyPressed(keyData.key);
}

Input& Input::Instance() {
  static Input input;
  return input;
}

void Input::AddCommand(const std::string& command, Input::Key key) {
  Input::Instance().inputMapping[command] = key;
}

}