#ifndef GAMEENGINE_CORE_SYSTEM_CONFIG_HPP
#define GAMEENGINE_CORE_SYSTEM_CONFIG_HPP
#include <nlohmann/json.hpp>

namespace engine {
struct Config {
protected:
  Config() {


    SetSetting<unsigned int>("windowSizeX", 1480);
    SetSetting<unsigned int>("windowSizeY", 720);

    SetSetting<unsigned char>("WindowBackgroundR", 159);
    SetSetting<unsigned char>("WindowBackgroundG", 159);
    SetSetting<unsigned char>("WindowBackgroundB", 159);

    values["engine"]["window"]["title"] = "Ecs Game Engine";
    values["engine"]["window"]["size"]["width"] = 1480;
    values["engine"]["window"]["size"]["height"] = 720;
    values["game"]["ball"]["radius"] = 10;
    values["game"]["ball"]["velocity"]["intensity"] = 60;
    values["game"]["paddle"]["size"]["width"] = 100;
    values["game"]["paddle"]["size"]["height"] = 20;
    values["game"]["paddle"]["velocity"]["relativeToResolution"] = 40;
    values["game"]["paddle"]["velocity"] = 40;
    values["game"]["brick"]["size"]["width"] = 48;
    values["game"]["brick"]["size"]["height"] = 48;
    values["game"]["brick"]["padding"]["x"] = 4;
    values["game"]["brick"]["padding"]["y"] = 4;

    auto displayWidth1Percent = GetSetting<unsigned int>("windowSizeX") / 100.f;
    auto displayHeight1Percent = GetSetting<unsigned int>("windowSizeY") / 100.f;

    SetSetting<float>("displayWidth1Percent", displayWidth1Percent);
    SetSetting<float>("displayHeight1Percent", displayHeight1Percent);

    ballVelocityRelativeToRes = ballVelocity * displayHeight1Percent;
    paddleVelocityRelativeToRes = paddleVelocity * displayWidth1Percent;
    values["game"]["ball"]["velocity"]["relativeToResolution"] = ballVelocityRelativeToRes;

    /* you can declare it also like this
     values = {
      {"engine", {
        {"window", {
          {"title", "Ecs game engine"},
          {"size", {
            {"width", 1480},
            {"height", 720},
            {"1percent", {
              {"width", displayWidth1Percent},
              {"height", displayHeight1Percent}
            }}
          }}
        }}
      }},
      {"game", {
        {"ball", {
          {"radius", 10},
          {"velocity", {
            {"relativeToResolution", ballVelocityRelativeToRes},
            {"simple", 60}
          }}
        }},
        {"paddle", {
          {"size", {
            {"width", 100},
            {"height", 20}
          }},
          {"velocity", {
            {"relativeToResolution", paddleVelocityRelativeToRes},
            {"simple", 40}
          }}
        }},
        {"brick", {
          {"size", {
            {"width", 48},
            {"height", 48}
          }},
          {"padding", {
            {"x", 4},
            {"y", 4}
          }}
        }}
      }}
    };
     */

  }

public:
  template<typename ValueType>
  void SetSetting(const std::string& settingKey, const ValueType& value) {
    values["game"][settingKey] = value;
  }

  template<typename ValueType>
  ValueType GetSetting(const std::string& settingKey) {
    return values["game"][settingKey];
  }

  static Config& Instance() {
    static Config database;
    return database;
  }

  nlohmann::json values;

  unsigned ballVelocity {60};
  unsigned paddleVelocity {40};
  int brickSize {48};
  int brickPadding {4};
  float ballVelocityRelativeToRes;
  float paddleVelocityRelativeToRes;

  std::string windowTitle {"Ecs game engine"};
};
}

#endif //GAMEENGINE_CORE_SYSTEM_CONFIG_HPP
