#ifndef ENGINE_RESOURCES_HPP
#define ENGINE_RESOURCES_HPP

#include "../FrameworkNaming.hpp"

#include <memory>

#include <entt/resource/loader.hpp>
#include <entt/resource/resource.hpp>

namespace engine {

struct TextureLoader final {
  using result_type = std::shared_ptr<engine::Texture>;
  result_type operator()(const std::string& path, const sf::IntRect& area = sf::IntRect()) const;
};

template<typename T> using ResourceHandle = entt::resource<T>;
template<typename T, typename L> using ResourceCache = entt::resource_cache<T, L>;
using TextureHandle = ResourceHandle<engine::Texture>;
using TextureCache = ResourceCache<engine::Texture, engine::TextureLoader>;


class Resources {

public:
  std::string GetPath(const std::string& fileName);
  TextureHandle GetTexture(const char* path, const sf::IntRect& rect = sf::IntRect());
  TextureHandle GetTexture(const std::string& path, const sf::IntRect& rect = sf::IntRect());
  TextureHandle GetTexture(const engine::HashedString& id, const sf::IntRect& area = sf::IntRect());
  static Resources& Instance();

private:
  Resources() = default;
  TextureCache textureCache;

};

}
#endif //ENGINE_RESOURCES_HPP
