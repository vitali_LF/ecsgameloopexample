#include <iostream>
#include "Resources.hpp"

namespace engine {

TextureLoader::result_type TextureLoader::operator()(const std::string& path, const sf::IntRect& area) const {
  auto image = std::make_shared<sf::Texture>();
  if (!image->loadFromFile(path, area)) {
    ERROR("Cant't load Texture at path " << path);
  }
  return image;
}

TextureHandle Resources::GetTexture(const char* path, const sf::IntRect& rect) {
  return GetTexture(std::string(path));
}

TextureHandle Resources::GetTexture(const std::string& path, const sf::IntRect& area) {
  entt::hashed_string hs{path.c_str()};
  textureCache.load(hs, "./" + path, area);
  const auto& handle = textureCache[hs];
  return handle;
}

TextureHandle Resources::GetTexture(const engine::HashedString& id, const sf::IntRect& area) {
  if (!textureCache.contains(id)) {
    auto path = "./" + std::string((const char*) id);
    textureCache.load(id, path, area); //FIXME get resource path at runtime
  }
  const TextureHandle& handle = textureCache[id];
  return handle;
}

Resources& Resources::Instance() {
  static Resources instance;
  return instance;
}

std::string Resources::GetPath(const std::string& fileName) {
  return "./resources/" + fileName;
}
}