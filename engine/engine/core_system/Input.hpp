#ifndef ENGINE_INPUT_HPP
#define ENGINE_INPUT_HPP

#include <map>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Event.hpp>

#include <engine/FrameworkNaming.hpp>


namespace engine {

using Keyboard = sf::Keyboard;

class Input {

  Input();

public:
  struct Key {
    engine::Keyboard::Key key;
    bool pressed;
  };

  static Input& Instance();

  static void AddCommand(const std::string& command, Input::Key key);

  void Update();

  bool IsKeyPressed(std::string command, unsigned player = 1);

public:
  engine::Signal<void(std::string, bool pressed)> onNewInput;

private:
  std::map<std::string, Input::Key> inputMapping;
};

}
#endif //ENGINE_INPUT_HPP
