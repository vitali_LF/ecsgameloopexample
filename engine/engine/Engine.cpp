#include "Engine.hpp"
#include "Module.hpp"

#include <nod/nod.hpp>
#include <engine/core_system/Config.hpp>
#include <engine/core_system/Input.hpp>

#include <memory>

namespace engine {

void ProcessEvents(engine::RenderWindow& window, engine::Game& game) {
  sf::Event event{};
  while (window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      // TODO: trigger window about to close event
      window.close();
    }

    // catch the resize events
    if (event.type == sf::Event::Resized)
    {
      // update the view to the new size of the window
      sf::FloatRect visibleArea(0, 0, static_cast<float>(event.size.width), static_cast<float>(event.size.height));
      window.setView(sf::View(visibleArea));

      engine::Config::Instance().SetSetting<unsigned int>("windowSizeX", event.size.width);
      engine::Config::Instance().SetSetting<unsigned int>("windowSizeY", event.size.height);

      auto displayWidth1Percent = engine::Config::Instance().GetSetting<unsigned int>("windowSizeX") / 100.f;
      auto displayHeight1Percent = engine::Config::Instance().GetSetting<unsigned int>("windowSizeY") / 100.f;

      engine::Config::Instance().SetSetting<float>("displayWidth1Percent", displayWidth1Percent);
      engine::Config::Instance().SetSetting<float>("displayHeight1Percent", displayHeight1Percent);
    }
  }
}

int Engine::Run() {
  _window = std::make_unique<engine::RenderWindow>(
    sf::VideoMode{engine::Config::Instance().GetSetting<unsigned int>("windowSizeX"),
                  engine::Config::Instance().GetSetting<unsigned int>("windowSizeY")},
    engine::Config::Instance().windowTitle
  );
  _window->setFramerateLimit(60);
  _window->setVerticalSyncEnabled(true);
  _game->SetWindow(_window);
  _game->Init();
  _game->StartGame();
  sf::Clock deltaClock;
  while (_window->isOpen()) {

    auto windowBackgroundR = engine::Config::Instance().GetSetting<unsigned char>("WindowBackgroundR");
    auto windowBackgroundG = engine::Config::Instance().GetSetting<unsigned char>("WindowBackgroundG");
    auto windowBackgroundB = engine::Config::Instance().GetSetting<unsigned char>("WindowBackgroundB");

    ProcessEvents(*_window.get(), *_game.get());

    engine::Input::Instance().Update();

    _window->clear(sf::Color(windowBackgroundR, windowBackgroundG, windowBackgroundB));

    auto dt = deltaClock.restart().asSeconds();

    _onStepSubscription(dt);

    if (_game) {
      _game->Update(dt);
    }

    for (const auto& module: _modules) {
      module->GetWindow().clear(sf::Color(windowBackgroundR, windowBackgroundG, windowBackgroundB));
      module->Update(dt);
      module->GetWindow().display();
    }

    _window->display();
  }
  return 0;
}

void Engine::SetGame(const std::shared_ptr<Game>& game) {
  _game = game;
  if (_window) {
    _game->SetWindow(_window);
  }
}

sf::RenderWindow& Engine::GetWindow() {
  return *_window;
}

void Engine::AddModules(const std::shared_ptr<Module>& module) {
  auto window = std::make_shared<sf::RenderWindow>(
    sf::VideoMode{(unsigned) module->GetSize().x, (unsigned) module->GetSize().y},
    module->GetTitle()
  );
  window->setFramerateLimit(60);
  window->setVerticalSyncEnabled(true);
  module->SetWindow(window);
  module->Init();
  _modules.push_back(module);
}
}