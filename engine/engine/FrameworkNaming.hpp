#ifndef ECSGAMELOOP_FRAMEWORKNAMING_HPP
#define ECSGAMELOOP_FRAMEWORKNAMING_HPP


#include <entt/core/hashed_string.hpp>
#include <entt/entity/registry.hpp>
#include <entt/entt.hpp>
#include <nod/nod.hpp>
#include <iostream>

// SFML FRAMEWORK
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>



namespace engine {
using Font = sf::Font;
using Text = sf::Text;
using Color = sf::Color;
using Sprite = sf::Sprite;
using Shape = sf::Shape;
using Texture = sf::Texture;
using RenderWindow = sf::RenderWindow;
using RectangleShape = sf::RectangleShape;

// Define the most common types
using Vector2i = sf::Vector2<int>;
using Vector2u = sf::Vector2<unsigned int>;
using Vector2f = sf::Vector2<float>;
}

namespace engine {
using HashedString = entt::hashed_string;
using Dispatcher = entt::dispatcher;
using Registry = entt::registry;
using Entity = entt::registry::entity_type;
template<typename T> using Signal = nod::signal<T>;
};

struct ColorHash {
  std::size_t operator()(const engine::Color& v) const {
    return std::hash<size_t>()(
      static_cast<size_t>(v.r) +
      static_cast<size_t>(v.g) +
      static_cast<size_t>(v.b) +
      static_cast<size_t>(v.a));
  }
};


#define LOG(msg) do { \
std::string filePath(__FILE__); \
std::size_t found = filePath.find_last_of("/\\"); \
std::string fileName = filePath.substr(found+1); \
  std::cout << "[" << fileName << "]:[" << __LINE__ /*<< "]:[" << __FUNCTION__  */ << "]: " << msg << '\n'; \
} while (0)

#define ERROR(msg) do { \
std::string filePath(__FILE__); \
std::size_t found = filePath.find_last_of("/\\"); \
std::string fileName = filePath.substr(found+1); \
  std::cerr << "[" << fileName << "]:[" << __LINE__ << "]:[" << __FUNCTION__  << "]: " << msg << '\n'; \
} while (0)

#define ERRORF(format, ...) do { \
std::string filePath(__FILE__); \
std::size_t found = filePath.find_last_of("/\\"); \
std::string fileName = filePath.substr(found+1); \
fprintf(stderr, (std::string("[") + fileName + std::string("]:[") + std::to_string(__LINE__) + std::string("]:[") + __FUNCTION__  + std::string("]: ")).c_str() ); \
fprintf(stderr, format,  ##__VA_ARGS__);\
fprintf(stderr, "\n"); } while (0)//

#endif //ECSGAMELOOP_FRAMEWORKNAMING_HPP
