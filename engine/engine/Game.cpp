#include "Game.hpp"
#include "Scene.hpp"
#include "core_system/Resources.hpp"
#include "core_system/Config.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <utility>

namespace engine {

Game::Game(std::shared_ptr<RenderWindow> window) : _window(std::move(window)) {
  _config = &Config::Instance();
}

Game::Game() : Game(nullptr) {
}

RenderWindow& Game::GetWindow() const {
  return *_window;
}

engine::Dispatcher& Game::GetEventDispatcher() {
  return _eventDispatcher;
}

void Game::Init() {
  _font.loadFromFile(Resources::Instance().GetPath("fonts/open-sans/OpenSans-Regular.ttf"));
  _text.setFont(_font);
  _text.setCharacterSize(32);
  for (auto&& scene: _scenes) {
    scene.second->Init();
  }
}

void Game::StartGame() {
  gameStarted();
}

void Game::Update(float s) {
  if (_currentScene) {
    _currentScene->Update(s);
  }
}

std::shared_ptr<Scene> Game::GetCurrentScene() {
  return _currentScene;
}

void Game::SetCurrentScene(const std::shared_ptr<Scene>& scene) {
  if (_currentScene) {
    _currentScene->Pause();
  }
  _currentScene = scene;
  _currentScene->Start();
}

void Game::AddScene(const std::shared_ptr<Scene>& scene) {
  if (HasScene(scene->GetName())) {
    LOG("It already has a scene with this name");
    return;
  } else {
    LOG("Added a scene with the name: " << scene->GetName());
    _scenes[scene->GetName()] = scene;
    //scene->Init();
  }
}

void Game::SetCurrentScene(const std::string& label) {
  if (HasScene(label)) {
    SetCurrentScene(_scenes[label]);
  } else {
    ERROR("SCENE NOT FOUND: " << label);
  }
}

void Game::SetWindow(const std::shared_ptr<RenderWindow>& window) {
  _window = window;
  LOG("Window set");
}

bool Game::HasScene(const std::string& label) {
  return tools::HasEntry(_scenes, label);
}

engine::Registry& Game::GetRegistry() {
  return _registry;
}

engine::Config& Game::GetConfig() {
  return *_config;
}

// For debug or just to try stuff out
void Game::DrawRect(float x, float y, float width, float height, float alpha) {
  static sf::RectangleShape r;
  r.setSize({(float) width, (float) height});
  r.setPosition({(float) x, (float) y});
  r.setFillColor(Color(0, 0, 0, (Uint8) (255 * alpha)));
  _window->draw(r);
}

void Game::DrawText(const std::string& text, Vector2 position, unsigned int size) {
  _text.setString(text);
  _text.setCharacterSize(size);
  _text.setFillColor(engine::Color::Black);
  _text.setPosition(position.x, position.y);
  _window->draw(_text);
}


}
