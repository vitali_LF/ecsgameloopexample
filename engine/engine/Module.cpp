#include "Module.hpp"

#include <utility>

namespace engine {

const Vector2& Module::GetSize() const {
  return _size;
}

const std::string& Module::GetTitle() const {
  return _title;
}

Module::Module(const Vector2& size, std::string  title) : _size(size), _title(std::move(title)) {}

Module::Module() = default;

}
