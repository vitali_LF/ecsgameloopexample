#ifndef ECSGAMELOOP_ENGINE_SCENE_H
#define ECSGAMELOOP_ENGINE_SCENE_H

#include "System.hpp"

#include <entt/entt.hpp>
#include <functional>

using namespace sf;

namespace engine {
class Game;
using SystemEndless = std::function<void(engine::Game& game, float dt)>;
using SystemFinite = std::function<void(engine::Game& game, float dt, bool& finished)>;

class Scene {

public:
  explicit Scene(Game& game, std::string  name);

  virtual void Init();
  virtual void Finish();
  virtual void Start();
  virtual void Pause();
  virtual void Update(float dt);

  virtual void ResizeEvent(const engine::Vector2& size);

public:
  std::string GetName();
  void AddSystem(const std::shared_ptr<System>& system);
  void AddSystemLambda(engine::SystemEndless system);
  void AddSystemLambdaFinite(engine::SystemFinite system);

protected:
  engine::Registry _registry;
  Game& _game;
  bool _running {true};
  std::string _name;
  std::vector<std::shared_ptr<System>> _systems;
  std::vector<engine::SystemEndless> _systemsEndless;
  std::vector<engine::SystemFinite> _systemsFinite;
};

}
#endif //ECSGAMELOOP_ENGINE_SCENE_H
