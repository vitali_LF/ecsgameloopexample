#include "System.hpp"
namespace engine {
System::System(Game& game, engine::Registry& registry)
  : _game(game), _registry(registry), _eventDispatcher(_game.GetEventDispatcher()) {}

bool System::HasFinished() const {
  return _finished;
}

void System::SetFinished(bool finished) {
  _finished = finished;
}

void System::Update(float dt) { }

}