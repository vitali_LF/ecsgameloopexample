#ifndef ENGINE_INCLUDES_HPP
#define ENGINE_INCLUDES_HPP

#include "FrameworkNaming.hpp"

#include "core_system/Resources.hpp"
#include "core_system/Config.hpp"
#include "ecs/components/Components.hpp"
#include "ecs/systems/Systems.hpp"
#include "tools/HelperFunctions.hpp"
#include "Game.hpp"
#include "Scene.hpp"
#include "System.hpp"



namespace engine {
  using GamePtr = std::shared_ptr<engine::Game>;
  using ScenePtr = std::shared_ptr<engine::Scene>;
}

#endif //ENGINE_INCLUDES_HPP
