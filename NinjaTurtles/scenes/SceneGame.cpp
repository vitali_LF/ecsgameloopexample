#include "SceneGame.hpp"
#include <components/Components.hpp>
#include <systems/Systems.hpp>
#include <engine/core_system/Input.hpp>
#include <engine/ecs/systems/CubeArray.hpp>
#include <engine/FrameworkIncludes.hpp>

namespace scenes {

SceneGame::SceneGame(engine::Game& game) : engine::Scene(game, "game") {
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundR", 2);
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundG", 26);
  engine::Config::Instance().SetSetting<unsigned char>("WindowBackgroundB", 51);
  engine::Config::Instance().SetSetting<unsigned int>("windowSizeX", 730);
  engine::Config::Instance().SetSetting<unsigned int>("windowSizeY", 720);
  _soundStart = std::make_shared<engine::Sound>("sounds/start.wav");
}

void SceneGame::Update(float dt) {
  if (!_running) return;
  engine::Scene::Update(dt);
  ProcessInput(dt);
}

void SceneGame::Start() {
  LOG("Start Game");
  Scene::Start();
  // _soundStart->Play();
}

void SceneGame::Init() {
  LOG("Init Game");
  InitEntts();
  InitSystems();
}

void SceneGame::Finish() {
  std::cout << "Finish Game" << '\n';
}

void SceneGame::InitSystems() {
  // Add Rendering Systems
  // AddSystemLambda(&game::ecs::systems::SystemSortDrawabe);
  // AddSystemLambda(&game::ecs::systems::SystemClicked);
  // AddSystemLambda(&game::ecs::systems::SystemDrawSprites);
  AddSystemLambda(&game::ecs::systems::SystemDrawAnimatedSprites);

  // Add Simulation Systems
  AddSystemLambda(&game::ecs::systems::SystemKeyboardCtrl);
  AddSystemLambda(&game::ecs::systems::SystemApply);
  AddSystemLambda(&game::ecs::systems::SystemPlaySound);
}


void SceneGame::InitEntts() {
  // TODO: move to prefabs
  game::ecs::systems::CreateEntities(_game, 0.0f);
}

void SceneGame::ProcessInput(float dt) {
  if (engine::Input::Instance().IsKeyPressed("menu")) {
    _game.SetCurrentScene("main_menu");
  }
}

}