cmake_minimum_required(VERSION 3.8.2)
project(NinjaTurtles)

add_executable(${PROJECT_NAME}
  ecs/components/Components.hpp
  ecs/prefabs/Prefabs.hpp
  ecs/prefabs/Prefabs.cpp
  ecs/systems/Systems.hpp
  ecs/systems/Systems.cpp
  ecs/systems/MousePositionSystem.cpp
  ecs/systems/MousePositionSystem.hpp
  scenes/SceneGame.cpp
  scenes/SceneGame.hpp
  scenes/SceneStart.hpp
  scenes/GameOver.hpp
  main.cpp)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/ecs)
target_link_libraries(${PROJECT_NAME} PUBLIC engine)

# Install target
install(TARGETS ${PROJECT_NAME} DESTINATION ${CMAKE_SOURCE_DIR}/bin)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/resources/ DESTINATION ${CMAKE_SOURCE_DIR}/bin/resources )

# Create symlink for developers
message(STATUS CMAKE_BINARY_DIR ${CMAKE_BINARY_DIR})
message(STATUS CMAKE_SOURCE_DIR ${CMAKE_SOURCE_DIR})
message(STATUS CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
message(STATUS CMAKE_HOME_DIRECTORY ${CMAKE_HOME_DIRECTORY})

set(source "${CMAKE_SOURCE_DIR}/resources" "${CMAKE_CURRENT_SOURCE_DIR}/resources")
set(dest "${CMAKE_CURRENT_BINARY_DIR}/resources")
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${source} ${dest})

add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy_if_different
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/openal32.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-system-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-network-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-graphics-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-audio-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-window-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-system-d-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-network-d-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-graphics-d-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-audio-d-2.dll"
  "${CMAKE_SOURCE_DIR}/engine/third_party/SFML-2.5.1/bin/sfml-window-d-2.dll"
  $<TARGET_FILE_DIR:${PROJECT_NAME}>)
