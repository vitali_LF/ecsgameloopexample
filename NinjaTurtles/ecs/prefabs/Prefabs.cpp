#include "Prefabs.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <components/Components.hpp>

engine::Entity CreateGameOverPage(engine::Registry& registry) {
  std::cout << "Game Over biatch" << std::endl;
  auto gameOverEntity = registry.create();
  auto size = sf::IntRect(0, 0, 800, 400);
  auto shape = std::make_shared<sf::RectangleShape>(sf::Vector2f{(float) size.width, (float) size.height});
  auto texture = std::make_shared<sf::Texture>();
  texture->loadFromFile("../resources/you-suck.jpg", size);
  shape->setTexture(texture.get());
  registry.emplace<game::ecs::components::DrawableShape>(gameOverEntity, shape, sf::Color::White, 1);
  registry.emplace<game::ecs::components::Texture2D>(gameOverEntity, texture, sf::RectangleShape{});
  registry.emplace<game::ecs::components::GameOverScreen>(gameOverEntity);
  return gameOverEntity;
}

engine::Entity CreateAnimatedSprite(engine::Registry& registry, const std::vector<sf::Texture>& textures,
                                    std::map<game::ecs::components::AnimationState, sf::Vector2i> animationStates,
                                    float x, float y, float speed, float scale, bool mainChar) {
  auto sprite = std::make_shared<sf::Sprite>();
  sprite->setTexture(textures[0]);
  auto bounds = sprite->getGlobalBounds();
  sprite->setPosition(x - bounds.width * 0.5f, y - bounds.height * 0.5f);
  sprite->scale(scale, scale);

  auto entity = registry.create();
  registry.emplace<game::ecs::components::Position>(entity, x, y);
  registry.emplace<game::ecs::components::Name>(entity, "animatedSprite");
  registry.emplace<game::ecs::components::AnimatedSpriteComponent>(entity, textures, sprite, 0, speed, 0);
  registry.emplace<game::ecs::components::AnimationStateComponent>(entity, animationStates, game::ecs::components::AnimationState::IDLE);
  registry.emplace<game::ecs::components::Size>(entity, bounds.width, bounds.height);
  registry.emplace<game::ecs::components::Box2DCollider>(entity, game::ecs::components::Size{bounds.width, bounds.height});
  registry.emplace<game::ecs::components::ClickableShape>(entity, false, false);
  if (mainChar) {
    registry.emplace<game::ecs::components::MainCharacter>(entity);
  }
  return entity;
}

#define MAP_TILE_SIZE 16

engine::Entity
CreateMapTile(engine::Registry& registry, sf::Texture& textureMap, int tileIndexX, int tileIndexY, float x, float y,
              float scale) {
  auto spriteSheetMap = std::make_shared<sf::Sprite>();
  spriteSheetMap->setTexture(textureMap);
  spriteSheetMap->setPosition(x, y);
  spriteSheetMap
    ->setTextureRect(sf::IntRect(tileIndexX * MAP_TILE_SIZE, tileIndexY * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE));
  spriteSheetMap->scale(scale, scale);

  auto entity = registry.create();
  auto& config = engine::Config::Instance();
  registry.emplace<game::ecs::components::Position>(entity, x, y);
  registry.emplace<game::ecs::components::Name>(entity, "mapTile");
  registry.emplace<game::ecs::components::SpriteComponent>(entity, spriteSheetMap, 0);
  // registry.emplace<game::ecs::components::Size>(entity, (float)config.brickSizeX, (float)config.brickSizeY);
  // registry.emplace<game::ecs::components::DrawableShape>(entity, std::make_shared<sf::RectangleShape>(sf::Vector2f{40.f,20.0f}), sf::Color(223,223,223));
  // registry.emplace<game::ecs::components::Box2DCollider>(entity, Size{(float)config.brickSizeX, (float)config.brickSizeY});
  return entity;
}