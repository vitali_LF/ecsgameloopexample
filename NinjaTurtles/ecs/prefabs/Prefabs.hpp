#include <engine/FrameworkNaming.hpp>
#include <components/Components.hpp>

engine::Entity CreateGameOverPage(engine::Registry& registry);

engine::Entity CreateAnimatedSprite(engine::Registry& registry, const std::vector<sf::Texture>& textures,
                                    std::map<game::ecs::components::AnimationState, sf::Vector2i> animationStates,
                                    float x, float y, float speed, float scale, bool mainChar);

engine::Entity CreateMapTile(engine::Registry& registry, sf::Texture& textureMap, int tileIndexX, int tileIndexY,
                             float x, float y, float scale = 1.0f);