#pragma once

#include <engine/FrameworkIncludes.hpp>
#include <components/Components.hpp>
#include <prefabs/Prefabs.hpp>
#include "SFML/Window/Keyboard.hpp"
#include "SFML/Window/Mouse.hpp"

#include <SFML/OpenGL.hpp>

namespace game::ecs::systems {

std::once_flag onceFlag;

void ResizeOpenGL(engine::Vector2 size) {
  // adjust the viewport when the window is resized
  glViewport(0, 0, size.x, size.y);
}

void InitOpenGL() {
  glEnable(GL_TEXTURE_2D);
}

void OpenGLSystem(engine::Game& game, float dt) {


  game.GetWindow().popGLStates();
  std::call_once(onceFlag, [] {
  });
  game.GetWindow().pushGLStates();

}

//SYTEM : Sound
void SystemPlaySound(engine::Game& game, float dt) {
  static bool loaded = false;
  static sf::SoundBuffer buffer;
  static sf::Sound sound;
  if (!loaded) {
    if (buffer.loadFromFile("resources/sounds/soundtest.wav")) {
      loaded = true;
      sound.setBuffer(buffer);
    }
  }

  game.GetRegistry().view<game::ecs::components::CoinFound>().each([&game](auto entity, auto& coinFound) {
    coinFound.found = false;
    game.GetRegistry().remove<game::ecs::components::CoinFound>(entity);
    if (sound.getStatus() != sf::Sound::Playing) {
      sound.play();
    }
    std::cout << "Coin found sound " << std::endl;
  });
}

// SYSTEM : APPLY CHANGES
void SystemApply(engine::Game& game, float dt) {
  game.GetRegistry()
      .view<game::ecs::components::DrawableShape, game::ecs::components::Position, game::ecs::components::Size>()
      .each([](auto entity, game::ecs::components::DrawableShape& drawable, game::ecs::components::Position pos,
               game::ecs::components::Size size) {
        drawable.shape->setPosition({pos.x, pos.y});
      });

  game.GetRegistry()
      .view<game::ecs::components::AnimatedSpriteComponent, game::ecs::components::Position>()
      .each([](auto entity, game::ecs::components::AnimatedSpriteComponent& animatedSprite,
               game::ecs::components::Position pos) {
        auto bounds = animatedSprite.sprite->getGlobalBounds();
        animatedSprite.sprite->setPosition({pos.x, pos.y});
      });
}

// SYSTEM : DRAW
void SystemDraw(engine::Game& game, float dt) {
  Shape* shape;
  game.GetRegistry().view<game::ecs::components::DrawableShape>().each([&](auto entity, auto& drawable) {
    shape = drawable.shape.get();
    shape->setFillColor(drawable.color);
    game.GetWindow().draw(*shape);
  });
}

void SystemDrawSprites(engine::Game& game, float dt) {
  game.GetRegistry().view<game::ecs::components::SpriteComponent>().each([&](auto entity, auto& spriteComponent) {
    auto& sprite = spriteComponent.sprite;
    game.GetWindow().draw(*sprite);
  });
}

void  SystemDrawAnimatedSprites(engine::Game& game, float dt) {
  const int SPRITE_SPEED_MS = 100;
  static float passedTime = 0.0f;
  passedTime += dt;

  game.GetRegistry()
      .view<game::ecs::components::AnimatedSpriteComponent, game::ecs::components::AnimationStateComponent>()
      .each([&](auto entity, auto& animatedSpriteComponent, auto& animationStateComponent) {
        auto sprite = animatedSpriteComponent.sprite;
        auto textures = animatedSpriteComponent.spriteTextures;
        int currentFrame = animatedSpriteComponent.currentFrame;
        float speed = animatedSpriteComponent.speed;
        game::ecs::components::AnimationState currentState = animationStateComponent.currentState;
        sf::Vector2i bounds = animationStateComponent.states[currentState];

        bool advanceAnimation = false;
        if (passedTime > speed) {
          passedTime = 0.0f;
          advanceAnimation = true;
        }
        if (advanceAnimation) {
          int frame = bounds.x + (currentFrame++ % bounds.y);
          sprite->setTexture(textures[frame]);
        }
        game.GetWindow().draw(*sprite);
      });
}

void SystemSortDrawabe(engine::Game& game, float dt) {
  game.GetRegistry().sort<game::ecs::components::DrawableShape>([](const auto& lhs, const auto& rhs) {
    return lhs.z < rhs.z;
  });
}

// SYSTEM : DRAW COLLIDER
void SystemDrawCollider(engine::Game& game, float dt) {
  RectangleShape r;
  r.setFillColor(Color::Transparent);
  r.setOutlineThickness(2);
  r.setOutlineColor(Color(100, 100, 100));

  game.GetRegistry().view<game::ecs::components::Box2DCollider, game::ecs::components::Position>()
      .each([&r, &game](auto entity, auto& coll, auto& pos) {
        r.setSize(Vector2f{coll.size.width, coll.size.hight});
        r.setPosition(Vector2f{pos.x, pos.y});
        game.GetWindow().draw(r);
      });
}

// SYSTEM : KEYBOARD FOR PADDLE
void SystemKeyboardCtrl(engine::Game& game, float dt) {
  game.GetRegistry()
      .view<
        game::ecs::components::MainCharacter,
        game::ecs::components::Position,
        game::ecs::components::AnimationStateComponent,
        game::ecs::components::AnimatedSpriteComponent>()
      .each([dt](auto entity,/* auto& mainCharacter, */auto& position, auto& animationState, auto& animatedSprite) {
        auto moveByInPercent = engine::Config::Instance().paddleVelocityRelativeToRes;
        sf::Vector2<float> input;
        if (sf::Keyboard::isKeyPressed(Keyboard::Key::Right)) input.x += 1;
        if (sf::Keyboard::isKeyPressed(Keyboard::Key::Left)) input.x -= 1;
        if (sf::Keyboard::isKeyPressed(Keyboard::Key::Up)) input.y -= 1;
        if (sf::Keyboard::isKeyPressed(Keyboard::Key::Down)) input.y += 1;

        // normalize

        float mag = std::sqrt(input.x * input.x + input.y * input.y);
        if (mag <= 0) {
          animationState.currentState = game::ecs::components::AnimationState::IDLE;
          return;
        }

        input.x /= mag;
        input.y /= mag;

        animationState.currentState = game::ecs::components::AnimationState::MOVING;

        sf::Vector2<float> scale = animatedSprite.sprite->getScale();
        sf::FloatRect bounds = animatedSprite.sprite->getLocalBounds();
        int w = (int) bounds.width;
        int h = (int) bounds.height;

        // flip
        if (input.x < 0) {
          animatedSprite.sprite->setTextureRect(sf::IntRect(w, 0, -w, h));
        } else {
          animatedSprite.sprite->setTextureRect(sf::IntRect(0, 0, w, h));
        }
        position.x += input.x * moveByInPercent * dt * 0.5f;
        position.y += input.y * moveByInPercent * dt * 0.5f;
      });
}


void SystemClicked(engine::Game& game, float dt) {
  auto mousePos = sf::Mouse::getPosition(game.GetWindow());
  game.GetRegistry()
      .view<game::ecs::components::Box2DCollider, game::ecs::components::Position, game::ecs::components::ClickableShape>()
      .each([&mousePos](auto entity,
                        game::ecs::components::Box2DCollider& box2DCollider,
                        game::ecs::components::Position& position,
                        game::ecs::components::ClickableShape& clickableShape) {
        auto rect = RectWrapper({position.x, position.y}, {box2DCollider.size.width, box2DCollider.size.hight});
        if (rect.Top() <= mousePos.y && rect.Bottom() >= mousePos.y && rect.Left() <= mousePos.x &&
            rect.Right() >= mousePos.x) {
          clickableShape.containsMouse = true;
          clickableShape.isClicked = sf::Mouse::isButtonPressed(sf::Mouse::Left);
          std::cout << "HERE: " << mousePos.x << ", " << mousePos.y << ";" << std::endl;
        } else {
          clickableShape.containsMouse = false;
          clickableShape.isClicked = false;
        }
      });
}

void CreateEntities(engine::Game& game, float dt) {

  // CREATE MAP
  const float TILE_SCALE = 4.0f;
  const float TILE_OFFSET = 4.0f;
  static sf::Texture textureMap;
  textureMap.loadFromFile("resources/sprites/environment/sheet.png");
  for(int x = 0; x < 8; x++) {
    for(int y = 0; y < 8; y++) {
      CreateMapTile(game.GetRegistry(), textureMap, 6, 1, x * (TILE_SCALE * 16 + TILE_OFFSET), y * (TILE_SCALE * 16 + TILE_OFFSET), TILE_SCALE);
    }
  }

  // CREATE ATTACKING TURTLE
  const float TURTLE_SCALE = 4.0f;
  std::vector<sf::Texture> texturesForSprite;

  std::map<game::ecs::components::AnimationState, sf::Vector2i> animationStates;
  std::vector<std::string> paths;

  int offset = 0;
  int count = 0;
  auto LoadTextures = [&](std::string path, std::vector<sf::Texture>& sprites) {
    // find all textures inside of the path
    std::vector<std::string> paths;
    for (const auto & entry : std::filesystem::directory_iterator(path)) {
      if (entry.exists()) {
        paths.push_back(entry.path().string());
      }
    }
    std::sort(paths.begin(), paths.end());

    offset += count;
    count = 0;
    for(const auto& path : paths) {
      sf::Texture tmp;
      tmp.loadFromFile(path);
      sprites.push_back(tmp);
      count++;
    }
  };

  // sprint textures
  LoadTextures("resources/sprites/turtle/sprint", texturesForSprite);
  animationStates[game::ecs::components::AnimationState::MOVING] = sf::Vector2i(offset, count);

  // attacking textures
  LoadTextures("resources/sprites/turtle/attackFinisher1", texturesForSprite);
  animationStates[game::ecs::components::AnimationState::ATTACKING] = sf::Vector2i(offset, count);

  // idle textures
  LoadTextures("resources/sprites/turtle/idle", texturesForSprite);
  animationStates[game::ecs::components::AnimationState::IDLE] = sf::Vector2i(offset, count);

  CreateAnimatedSprite(game.GetRegistry(), texturesForSprite, animationStates, 160.0f, 160.0f, 100.0f, TURTLE_SCALE, true);
}
void CreateEntities2(engine::Game& game, float dt) {
  // CREATE MAP
  const float TILE_SCALE = 4.0f;
  const float TILE_OFFSET = 4.0f;
  static sf::Texture textureMap;
  textureMap.loadFromFile("resources/sprites/environment/sheet.png");
  for (int x = 0; x < 8; x++) {
    for (int y = 0; y < 8; y++) {
      CreateMapTile(game.GetRegistry(), textureMap, x, y, x * (TILE_SCALE * 16 + TILE_OFFSET),
                    y * (TILE_SCALE * 16 + TILE_OFFSET), TILE_SCALE);
    }
  }

  // CREATE ATTACKING TURTLE
  const float TURTLE_SCALE = 4.0f;
  std::vector<sf::Texture> texturesForSprite;
  std::map<game::ecs::components::AnimationState, sf::Vector2i> animationStates;

  auto LoadTextures = [](const std::string& folderPath, std::vector<sf::Texture>& sprites) -> int {
    int count = 0;
    // find all textures inside the folder path
    std::vector<std::string> paths;
    for (const auto& entry: std::filesystem::directory_iterator(folderPath)) {
      if (entry.exists()) {
        paths.push_back(entry.path().string());
      }
    }
    std::sort(paths.begin(), paths.end());

    for (const auto& path: paths) {
      sf::Texture tmp;
      tmp.loadFromFile(path);
      sprites.push_back(tmp);
      count++;
    }
    return count;
  };

  int offset = 0;
  // sprint textures
  auto sprintCount = LoadTextures("resources/sprites/turtle/sprint", texturesForSprite);
  offset += sprintCount;
  animationStates[game::ecs::components::AnimationState::MOVING] = sf::Vector2i(offset, sprintCount);

  // attacking textures
  auto attackCount = LoadTextures("resources/sprites/turtle/attackFinisher1", texturesForSprite);
  offset += attackCount;
  animationStates[game::ecs::components::AnimationState::ATTACKING] = sf::Vector2i(offset, attackCount);

  // idle textures
  auto idleCount = LoadTextures("resources/sprites/turtle/idle", texturesForSprite);
  offset += idleCount;
  animationStates[game::ecs::components::AnimationState::IDLE] = sf::Vector2i(offset, idleCount);

  CreateAnimatedSprite(game.GetRegistry(), texturesForSprite, animationStates, 160.0f, 160.0f, 100.0f, TURTLE_SCALE,
                       true);
}

}
