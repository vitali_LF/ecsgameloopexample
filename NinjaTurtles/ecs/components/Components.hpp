#pragma once

#include <engine/FrameworkIncludes.hpp>

namespace game::ecs::components {


struct Position {
  float x;
  float y;
};

struct Velocity {
  float dx;
  float dy;
};

struct Size {
  float width;
  float hight;
};

class RectWrapper {
  Position& pos;
  Size& size;
public:
  RectWrapper(Position &pos, Size &size) : pos(pos), size(size) {}
  float Right() const { return pos.x + size.width; }
  float Left() const { return pos.x; }
  float Bottom() const { return pos.y + size.hight; }
  float Top() const { return pos.y; }
};

struct Box2DCollider {
  Size size;
};

struct ClickableShape {
  bool containsMouse;
  bool isClicked;
};

struct DrawableShape {
  std::shared_ptr<sf::Shape> shape = nullptr;
  sf::Color color = sf::Color::White;
  int z = 0;
};

struct SpriteComponent {
  std::shared_ptr<sf::Sprite> sprite;
  int z = 0;
};

struct AnimatedSpriteComponent {
  std::vector<sf::Texture> spriteTextures;
  std::shared_ptr<sf::Sprite> sprite;
  // int framesStart = 1;
  // int framesEnd = 1;
  int currentFrame = 0;
  float speed = 1;
  int z = 0;
};

enum AnimationState {IDLE, MOVING, ATTACKING};
struct AnimationStateComponent {
  std::map<AnimationState, sf::Vector2i> states;
  AnimationState currentState;
};

struct Name {
  std::string value;
};

struct Texture2D {
  std::shared_ptr<sf::Texture> texture;
  sf::RectangleShape shape;
};

struct CoinFound {
  bool found = true;
};

struct Paddle{};
struct PaddleTag{};
struct Circle{};
struct Brick{};
struct BallTag{};
struct GameOverScreen{};
struct Enemy {};
struct MainCharacter{};

}