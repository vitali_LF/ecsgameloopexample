#include <engine/Engine.hpp>
#include "scenes/SceneStart.hpp"
#include "scenes/SceneGame.hpp"
#include "scenes/GameOver.hpp"
#include <nlohmann/json.hpp>



void ProcessEvents(engine::Game& game) {
  Event event{};
  auto& window = game.GetWindow();
  if (Input::Instance().IsKeyPressed("quit")) window.close();
  while (window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) window.close();

    // catch the resize events
    if (event.type == sf::Event::Resized)
    {
      // update the view to the new size of the window
      sf::FloatRect visibleArea(0, 0, static_cast<float>(event.size.width), static_cast<float>(event.size.height));
      window.setView(sf::View(visibleArea));
      if (auto currentScene = game.GetCurrentScene()) {
        currentScene
          ->ResizeEvent(engine::Vector2{static_cast<float>(event.size.width), static_cast<float>(event.size.height)});
      }
    }
  }
}

int main(int argc, const char **argv) {
  /*** Create Engine. It manages the main loop ***/
  engine::Engine engine;

  engine::GamePtr game = std::make_shared<engine::Game>();
  engine._onStepSubscription.connect([game](float) { ProcessEvents(*game); });
  engine.SetGame(game);

  engine::ScenePtr mainMenuScene = std::make_shared<scenes::SceneStart>(*game);
  engine::ScenePtr gameScene = std::make_shared<scenes::SceneGame>(*game);
  engine::ScenePtr gameOver = std::make_shared<scenes::GameOver>(*game);

  game->AddScene(mainMenuScene);
  game->AddScene(gameScene);
  game->AddScene(gameOver);

  game->SetCurrentScene(gameScene->GetName());

  // engine.AddModules(std::make_shared<Module>());

  engine.Run();
  return 0;
}
